/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./resources/assets/user_side/js/scripts.js":
/*!**************************************************!*\
  !*** ./resources/assets/user_side/js/scripts.js ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

/**
 * Created by hass on 10/6/2017.
 */
window.onscroll = function () {
  scroll_func();
};

function scroll_func() {
  if (document.body.scrollTop > 30 || document.documentElement.scrollTop > 30) {
    document.getElementById("scroll-top").style.display = "block";
    document.getElementById("steaky_nav").style.top = "0px";
  } else {
    document.getElementById("scroll-top").style.display = "none";
    document.getElementById("steaky_nav").style.top = "-40px";
  }
}

var bgImages = ["green_band-wallpaper-1920x1080.jpg", "alone_in_the_street-wallpaper-1920x1080.jpg", "blue_texture_2-wallpaper-1920x1080.jpg"];
var bg_pointer = 0;

function st_slider(base_url) {
  document.getElementById("header").style.backgroundImage = "url('" + base_url + "/pics/" + bgImages[0] + "'), url('" + base_url + "/pics/" + bgImages[1] + "'), url('" + base_url + "/pics/" + bgImages[2] + "')";
  setInterval("slider()", 7000);
}

function slider() {
  if (bg_pointer < bgImages.length - 1) {
    var bg_position = [];

    for (var i = 0; i < bgImages.length; i++) {
      i <= bg_pointer ? bg_position.push("-1360px 0") : bg_position.push("0 0");
    }

    document.getElementById("header").style.backgroundPosition = bg_position.join(',');
    bg_pointer++;
  } else {
    var _bg_position = [];

    for (var _i = 0; _i < bgImages.length; _i++) {
      _bg_position.push("0 0");
    }

    document.getElementById("header").style.backgroundPosition = _bg_position.join(',');
    bg_pointer = 0;
  }
}

/***/ }),

/***/ "./resources/assets/user_side/scss/card_pages/styles.scss":
/*!****************************************************************!*\
  !*** ./resources/assets/user_side/scss/card_pages/styles.scss ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),

/***/ "./resources/assets/user_side/scss/comparision/styles.scss":
/*!*****************************************************************!*\
  !*** ./resources/assets/user_side/scss/comparision/styles.scss ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),

/***/ "./resources/assets/user_side/scss/home_page/styles.scss":
/*!***************************************************************!*\
  !*** ./resources/assets/user_side/scss/home_page/styles.scss ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),

/***/ "./resources/assets/user_side/scss/overall/styles.scss":
/*!*************************************************************!*\
  !*** ./resources/assets/user_side/scss/overall/styles.scss ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),

/***/ "./resources/assets/user_side/scss/page_data/styles.scss":
/*!***************************************************************!*\
  !*** ./resources/assets/user_side/scss/page_data/styles.scss ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),

/***/ 0:
/*!*************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** multi ./resources/assets/user_side/js/scripts.js ./resources/assets/user_side/scss/overall/styles.scss ./resources/assets/user_side/scss/card_pages/styles.scss ./resources/assets/user_side/scss/comparision/styles.scss ./resources/assets/user_side/scss/home_page/styles.scss ./resources/assets/user_side/scss/page_data/styles.scss ***!
  \*************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(/*! C:\xampp\htdocs\social_network\resources\assets\user_side\js\scripts.js */"./resources/assets/user_side/js/scripts.js");
__webpack_require__(/*! C:\xampp\htdocs\social_network\resources\assets\user_side\scss\overall\styles.scss */"./resources/assets/user_side/scss/overall/styles.scss");
__webpack_require__(/*! C:\xampp\htdocs\social_network\resources\assets\user_side\scss\card_pages\styles.scss */"./resources/assets/user_side/scss/card_pages/styles.scss");
__webpack_require__(/*! C:\xampp\htdocs\social_network\resources\assets\user_side\scss\comparision\styles.scss */"./resources/assets/user_side/scss/comparision/styles.scss");
__webpack_require__(/*! C:\xampp\htdocs\social_network\resources\assets\user_side\scss\home_page\styles.scss */"./resources/assets/user_side/scss/home_page/styles.scss");
module.exports = __webpack_require__(/*! C:\xampp\htdocs\social_network\resources\assets\user_side\scss\page_data\styles.scss */"./resources/assets/user_side/scss/page_data/styles.scss");


/***/ })

/******/ });