<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class TestCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'TEST_command {name} {--T|type=line}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'this command is for test {name} {--T|type=line}';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $name = $this->argument('name');
        $name_show_type = $this->option('type');
        switch ($name_show_type)
        {
            case 'info' :
                $this->info('welcome '.$name.' to command test ---- info');
                break;
            case 'line' :
                $this->line('welcome '.$name.' to command test ---- line');
                break;
            case 'comment' :
                $this->comment('welcome '.$name.' to command test ---- comment');
                break;
            case 'question' :
                $this->question('welcome '.$name.' to command test ---- question');
                break;
            default :
                $this->error('name cant be empty');
                $name = $this->ask('What is your name?');
                $type = $this->choice('What type do u like?', ['info', 'line' , 'comment' , 'question'], 'line');
                $this->{$type}('welcome '.$name.' to command test ---- question');
                break;
        }
    }
}
