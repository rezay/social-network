<?php

namespace App\Console\Commands;

use App\Helpers\Constants;
use App\Models\Interest_rate;
use App\Models\Page;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use InstagramScraper\Exception\InstagramException;
use InstagramScraper\Exception\InstagramNotFoundException;
use InstagramScraper\Instagram;

class InstagramInterestRates extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'Cron:Instagram-interest-rates';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'this command calculate interest rates';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     * @throws InstagramException
     * @throws \InstagramScraper\Exception\InstagramAuthException
     * @throws \phpFastCache\Exceptions\phpFastCacheDriverCheckException
     */
    public function handle()
    {
        $instagram_credential = config()->get('custom.instagram_credential')[array_rand(config()->get('custom.instagram_credential'))];
        $instagram = Instagram::withCredentials($instagram_credential['username'], $instagram_credential['password']);
        $instagram->login();
        $pages = Page::where("instagram_id" ,'!=', NULL)->get();
        $messages = [];

        $today_interest_rates = Interest_rate::select(['accountable_id'])
            ->where('social_network_id' , Constants::SOCIAL_NETWORK['INSTAGRAM']['ID'])
            ->where('accountable_type' , Page::Class)
            ->where('created_at' , '>' , Carbon::yesterday())
            ->get()->toArray();
        foreach ($pages as $page)
        {
            if( ! in_array($page->id , $today_interest_rates))
            {
                try{
                    $account = $instagram->getAccount($page->username);
                    $total_video_views = 0;
                    $total_likes = 0;
                    $total_comments = 0;
                    $medias = $account->getMedias();
                    foreach($medias as $media)
                    {
                        $total_video_views += $media->getVideoViews();
                        $total_comments += $media->getCommentsCount();
                        $total_likes += $media->getLikesCount();
                    }
                    Interest_rate::insert([
                        [
                            'title' => Constants::SOCIAL_NETWORK['INSTAGRAM']['INTEREST_RATES']['BY_COMMENTS_AND_LIKES']['TITLE'],
                            'type' => Constants::SOCIAL_NETWORK['INSTAGRAM']['INTEREST_RATES']['BY_COMMENTS_AND_LIKES']['TYPE'],
                            'rate' => ($total_comments+$total_likes)/$account->getFollowedByCount(),
                            'social_network_id' => Constants::SOCIAL_NETWORK['INSTAGRAM']['ID'],
                            'accountable_type' => Page::Class,
                            'accountable_id' => $page->id,
                            'created_at' => date("Y-m-d H:i:s"),
                            'timestamp' => time(),
                        ],[
                            'title' => Constants::SOCIAL_NETWORK['INSTAGRAM']['INTEREST_RATES']['BY_VIDEOS_VIEW']['TITLE'],
                            'type' => Constants::SOCIAL_NETWORK['INSTAGRAM']['INTEREST_RATES']['BY_VIDEOS_VIEW']['TYPE'],
                            'rate' => ($total_video_views)/$account->getFollowedByCount(),
                            'social_network_id' => Constants::SOCIAL_NETWORK['INSTAGRAM']['ID'],
                            'accountable_type' => Page::Class,
                            'accountable_id' => $page->id,
                            'created_at' => date("Y-m-d H:i:s"),
                            'timestamp' => time(),
                        ]
                    ]);
                }
                catch (InstagramException $e)
                {
                    $messages['errors'][$page->username] = 'cant access';
                }
                catch (InstagramNotFoundException $e)
                {
//                $messages['errors'][$page->username] = $e->getMessage();
                    $messages['errors'][$page->username] = 'server error';
                }
            }
        }
        $messages['overall_message'] = 'operation_done';
        $response =  response()->json(['http_code' => 200,'messages'=>$messages,'data'=>[]]);
        Log::channel('daily')->info($response);
    }
}
