<?php

namespace App\Http\Controllers;

use App\Helpers\Constants;
use App\Models\Interest_rate;
use App\Models\Page;
use Carbon\Carbon;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Log;
use InstagramScraper\Exception\InstagramAuthException;
use InstagramScraper\Exception\InstagramException;
use InstagramScraper\Exception\InstagramNotFoundException;
use InstagramScraper\Instagram;
use phpFastCache\Exceptions\phpFastCacheDriverCheckException;


class Controller extends BaseController
{
    public function __construct()
    {
        if( ! isset($this->instagram))
        {
            $instagram_credential = config()->get('custom.instagram_credential')[array_rand(config()->get('custom.instagram_credential'))];
            try {
                $this->instagram = Instagram::withCredentials($instagram_credential['username'], $instagram_credential['password']);
                $this->instagram->login();
            } catch (phpFastCacheDriverCheckException $e) {

            } catch (InstagramAuthException $e) {

            } catch (InstagramException $e) {

            }
        }
    }
    private $instagram;

    protected function get_instagram()
    {
        return $this->instagram;
    }

    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected function prepare_response(int $http_code , $messages , array $data)
    {
        return response()->json(['http_code' => $http_code,'messages'=>$messages,'data'=>$data]);
    }

    public function test()
    {
        $instagram_credential = config()->get('custom.instagram_credential')[array_rand(config()->get('custom.instagram_credential'))];
        $instagram = Instagram::withCredentials($instagram_credential['username'], $instagram_credential['password']);
        $instagram->login();
        $pages = Page::where("instagram_id" ,'!=', NULL)->get();
        $messages = [];

        $today_interest_rates = Interest_rate::select(['accountable_id'])
            ->where('social_network_id' , Constants::SOCIAL_NETWORK['INSTAGRAM']['ID'])
            ->where('accountable_type' , Page::Class)
            ->where('created_at' , '>' , Carbon::yesterday())
            ->get()->toArray();
        dd($today_interest_rates);
        foreach ($pages as $page)
        {
            if( ! in_array($page->id , $today_interest_rates))
            {
                try{
                    $account = $instagram->getAccount($page->username);
                    $total_video_views = 0;
                    $total_likes = 0;
                    $total_comments = 0;
                    $medias = $account->getMedias();
                    foreach($medias as $media)
                    {
                        $total_video_views += $media->getVideoViews();
                        $total_comments += $media->getCommentsCount();
                        $total_likes += $media->getLikesCount();
                    }
                    Interest_rate::insert([
                        [
                            'title' => Constants::SOCIAL_NETWORK['INSTAGRAM']['INTEREST_RATES']['BY_COMMENTS_AND_LIKES']['TITLE'],
                            'type' => Constants::SOCIAL_NETWORK['INSTAGRAM']['INTEREST_RATES']['BY_COMMENTS_AND_LIKES']['TYPE'],
                            'rate' => ($total_comments+$total_likes)/$account->getFollowedByCount(),
                            'social_network_id' => Constants::SOCIAL_NETWORK['INSTAGRAM']['ID'],
                            'accountable_type' => Page::Class,
                            'accountable_id' => $page->id,
                            'date' => date('Y-m-d '),
                            'created_at' => time(),
                        ],[
                            'title' => Constants::SOCIAL_NETWORK['INSTAGRAM']['INTEREST_RATES']['BY_VIDEOS_VIEW']['TITLE'],
                            'type' => Constants::SOCIAL_NETWORK['INSTAGRAM']['INTEREST_RATES']['BY_VIDEOS_VIEW']['TYPE'],
                            'rate' => ($total_video_views)/$account->getFollowedByCount(),
                            'social_network_id' => Constants::SOCIAL_NETWORK['INSTAGRAM']['ID'],
                            'accountable_type' => Page::Class,
                            'accountable_id' => $page->id,
                            'date' => date('Y-m-d'),
                            'created_at' => time(),
                        ]
                    ]);
                }
                catch (InstagramException $e)
                {
                    $messages['errors'][$page->username] = 'cant access';
                }
                catch (InstagramNotFoundException $e)
                {
//                $messages['errors'][$page->username] = $e->getMessage();
                    $messages['errors'][$page->username] = 'server error';
                }
            }
        }
        $messages['overall_message'] = 'operation_done';
        $response =  response()->json(['http_code' => 200,'messages'=>$messages,'data'=>[]]);
        Log::channel('daily')->info($response);
    }
}
