<?php

namespace App\Http\Controllers\api;

use App\Helpers\Constants;
use App\Models\Every_day_data;
use App\Models\Interest_rate;
use App\Models\Page_media;
use ErrorException;
use Illuminate\Http\Request;
use App\Http\Controllers\controller;
use App\Models\Page;
use App\Models\Page_data;
use Carbon\Carbon;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Support\Facades\DB;
use InstagramScraper\Exception\InstagramException;
use InstagramScraper\Exception\InstagramNotFoundException;

class PageDataController extends Controller
{
    private $instagram;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $page_data = Page::where('id' , $id)->with(['page_data'])->get()->first()->toArray();
        return $this->prepare_response(200 , 'successful_get' , $page_data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function store_new_data(Request $request)
    {
        $media = $request->input('media');
        try
        {
            $every_day_data = Every_day_data::where('date' , date('Y-m-d'))
                ->whereRaw('total_records' , 'successful_records + deleted_accounts')
                ->orderBy('id', 'desc')
                ->get()->first();
            if($every_day_data && $every_day_data->failed_records == 0)
            {
                return $this->prepare_response(200,'today`s data is complete' , []);
            }
            else
            {
                if($every_day_data && $every_day_data->used_method == 'api')
                {
                    return $this->library($media);
                }
                else
                {
                    return $this->api($media);
                }
            }
        }
        catch(\Illuminate\Database\QueryException $e)
        {
//            $e->getMessage();
            return $this->prepare_response(500,'Server Error Please Try Again' , []);
        }
    }

    private function api($media)
    {
        $pages = Page::where("instagram_id" ,'!=', NULL)->get();
        $total_records = count($pages);
        $successful_records = 0;
        $deleted_accounts = 0;
        $messages = [];
        $last_successful_record_id = 'all_failed';

        $today_data = Page_data::select(['page_id'])
            ->where('created_at' , '>=' , strtotime(date('Y-m-d', time()). '00:00:00'))
            ->groupBy('page_id')
            ->get()->toArray();
        $today_data = array_column($today_data , 'page_id');
        $client = new Client();
        foreach ($pages as $page)
        {
            try
            {
                if( ! in_array($page->id , $today_data))
                {
                    DB::beginTransaction();
                    $response = $client->request('GET', "https://www.instagram.com/$page->username/?__a=1" , ['headers' => ['Accept'     => 'application/json']]);
                    if ($response->getStatusCode() == 200) {
                        $account = json_decode($response->getBody()->getContents());
                        if (isset($account->graphql->user) && ($account->graphql->user !== null)) {
                            $account = $account->graphql->user;
                            $data = [
                                'page_id' => $page->id,
                                'full_name' => $account->full_name,
                                'profile_pic_url' => $account->profile_pic_url,
                                'profile_pic_url_hd' => $account->profile_pic_url_hd,
                                'biography' => $account->biography,
                                'external_url' => $account->external_url,
                                'follows_count' => $account->edge_follow->count,
                                'followed_by_count' => $account->edge_followed_by->count,
                                'media_count' => $account->edge_owner_to_timeline_media->count,
                                'is_private' => $account->is_private,
                                'is_verified' => $account->is_verified,
                                'is_loaded' => 1,
                                'created_at' => time(),
                            ];
                            $last_successful_record = Page_data::create($data);
                            $last_successful_record_id = $last_successful_record->id;
                            if($media)
                            {
                                $medias = array_slice($account->edge_owner_to_timeline_media->edges , 0 , 4);
                                $media_datas = [];
                                foreach($medias as $media)
                                {
                                    $media = $media->node;
                                    $media_data = [
                                        'page_id' => $page->id,
                                        'created_time' => $media->taken_at_timestamp,
                                        'type' => ($media->is_video) ? 'video' : 'image',
                                        'short_code' => $media->shortcode,
                                        'video_views' => ($media->is_video) ? $media->video_view_count : 0,
                                        'likes_count' => $media->edge_media_preview_like->count,
                                        'comments_count' => $media->edge_media_to_comment->count,
                                        'caption' => (isset($media->edge_media_to_caption->edges[0])) ? $media->edge_media_to_caption->edges[0]->node->text : '',
                                        'image_thumbnail_url' => $media->thumbnail_src,
                                        'image_high_resolution_url' => $media->display_url,
                                        'is_caption_edited' => false,
                                        'is_ad' => false,
                                        'created_at' => time(),
                                        'created_at_date' => date('Y-m-d')
                                    ];
                                    $media_datas[] = $media_data;
                                }
                                Page_media::insert($media_datas);
                            }
                        }
                        else
                        {
                            throw new ErrorException('cant access');
                        }
                    }
                    else
                    {
                        throw new ErrorException('cant access');
                    }
                }
                DB::commit();
                $successful_records += 1;
            }
            catch(ErrorException $e)
            {
                DB::rollBack();
                $messages['errors'][$page->username] = $e->getMessage();
            }
            catch(GuzzleException $e)
            {
                DB::rollBack();
                $deleted_accounts += 1 ;
                $messages['errors'][$page->username] = 'this username is not valid';
            }

        }
        $data = [
            "total_pages" => $total_records,
            "successful_records" => $successful_records,
            "failed_records" => ($total_records-$successful_records-$deleted_accounts),
            "deleted_accounts" => $deleted_accounts,
            "last_successful_record_id" => $last_successful_record_id
        ];
        $messages['overall_message'] = 'operation_done';
        Every_day_data::create([
            'social_network_id' => 1,
            'date' => date('Y-m-d'),
            'total_records' => $total_records,
            'successful_records' => $successful_records,
            'failed_records' => ($total_records-$successful_records-$deleted_accounts),
            'deleted_accounts' => $deleted_accounts,
            'used_method' => 'api'
        ]);

        return $this->prepare_response(200,$messages , $data);

    }

    private function library($media)
    {
        $this->instagram = $this->get_instagram();
        $pages = Page::where("instagram_id" ,'!=', NULL)->get();
        $total_records = count($pages);
        $successful_records = 0;
        $deleted_accounts = 0;
        $messages = [];
        $last_successful_record_id = 'all_failed';

        $today_data = Page_data::select(['page_id'])
            ->where('created_at' , '>=' , strtotime(date('Y-m-d', time()). '00:00:00'))
            ->get()->toArray();
        $today_data = array_column($today_data , 'page_id');
        foreach ($pages as $page)
        {
            try{
                if( ! in_array($page->id , $today_data))
                {
                    DB::beginTransaction();
                    $account = $this->instagram->getAccount($page->username);
                    $data = [
                        'page_id' => $page->id,
                        'full_name' => $account->getFullName(),
                        'profile_pic_url' => $account->getProfilePicUrl(),
                        'profile_pic_url_hd' => $account->getProfilePicUrlHd(),
                        'biography' => $account->getBiography(),
                        'external_url' => $account->getExternalUrl(),
                        'follows_count' => $account->getFollowsCount(),
                        'followed_by_count' => $account->getFollowedByCount(),
                        'media_count' => $account->getMediaCount(),
                        'is_private' => $account->isPrivate(),
                        'is_verified' => $account->isVerified(),
                        'is_loaded' => $account->isLoaded(),
                        'created_at' => time(),
                    ];
                    $last_successful_record = Page_data::create($data);
                    $last_successful_record_id = $last_successful_record->id;
                    if($media)
                    {
                        $medias = array_slice($account->getMedias() , 0 , 4);
                        $media_datas = [];
                        foreach($medias as $media)
                        {
                            $media_data = [
                                'page_id' => $page->id,
                                'created_time' => $media->getCreatedTime(),
                                'type' => $media->getType(),
                                'short_code' => $media->getShortCode(),
                                'video_views' => $media->getVideoViews(),
                                'likes_count' => $media->getLikesCount(),
                                'comments_count' => $media->getCommentsCount(),
                                'caption' => $media->getCaption(),
                                'image_thumbnail_url' => $media->getImageThumbnailUrl(),
                                'image_high_resolution_url' => $media->getImageHighResolutionUrl(),
                                'is_caption_edited' => $media->isCaptionEdited(),
                                'is_ad' => $media->isAd(),
                                'created_at' => time(),
                                'created_at_date' => date('Y-m-d')
                            ];
                            $media_datas[] = $media_data;
                        }
                        Page_media::insert($media_datas);
                    }
                    DB::commit();
                }
                $successful_records += 1;
            }
            catch (InstagramException $e)
            {
//                    $messages['errors'][$page->username] = $e->getMessage();
                $messages['errors'][$page->username] = 'cant access';
//                    return $this->prepare_response(500,$e->getMessage() , []);
                DB::rollBack();
            }
            catch (InstagramNotFoundException $e)
            {
                $deleted_accounts += 1 ;
                $messages['errors'][$page->username] = $e->getMessage();
//                    return $this->prepare_response(401,$e->getMessage() , []);
                DB::rollBack();
            }

        }
        $data = [
            "total_pages" => $total_records,
            "successful_records" => $successful_records,
            "failed_records" => ($total_records-$successful_records-$deleted_accounts),
            "deleted_accounts" => $deleted_accounts,
            "last_successful_record_id" => $last_successful_record_id
        ];
        $messages['overall_message'] = 'operation_done';
        Every_day_data::create([
            'social_network_id' => 1,
            'date' => date('Y-m-d'),
            'total_records' => $total_records,
            'successful_records' => $successful_records,
            'failed_records' => ($total_records-$successful_records-$deleted_accounts),
            'deleted_accounts' => $deleted_accounts,
            'used_method' => 'library'
        ]);

        return $this->prepare_response(200,$messages , $data);

    }

    public function remove_duplicates()
    {
//        $person_data = Person_data::
    }
}
