<?php

namespace App\Http\Controllers\api;

use App\Helpers\Constants;
use App\Models\Interest_rate;
use App\Models\Page;
use App\Models\Page_data;
use App\Http\Controllers\Controller;
use App\Models\page_type;
use App\Models\page_view;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use InstagramScraper\Exception\InstagramException;
use InstagramScraper\Exception\InstagramNotFoundException;

class PageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     * @throws \InstagramScraper\Exception\InstagramAuthException
     * @throws \InstagramScraper\Exception\InstagramException
     * @throws \InstagramScraper\Exception\InstagramNotFoundException
     * @throws \phpFastCache\Exceptions\phpFastCacheDriverCheckException
     */
    public function store(Request $request)
    {
        $instagram = $this->get_instagram();
        $inputs = $request->input();
        $messages = [];
        $result = [];
        foreach($inputs as $input)
        {
            $page = [
                'username' => $input['username'],
                'instagram_id' => NULL,
            ];
            $types_id = explode(',' , $input['types_id']);
            try
            {
                $account = $instagram->getAccount($input['username']);
            }
            catch (InstagramException $e)
            {
                //$messages[$input['username']] = $e->getMessage();
                return $this->prepare_response(500,$e->getMessage() , []);
            }
            catch (InstagramNotFoundException $e)
            {
                $messages[$input['username']] = $e->getMessage();
                //return $this->prepare_response(401,$e->getMessage() , []);
            }


            $page['instagram_id'] = $account->getId();
            $new_page = Page::firstorcreate(['username' => $page['username']] , $page);
            if($new_page->wasRecentlyCreated)
            {
                foreach($types_id as $type_id)
                {
                    page_type::firstorcreate(['page_id' => $new_page->id , 'type_id' => $type_id]);
                }
                $data = [
                    'page_id' => $new_page->id,
                    'full_name' => $account->getFullName(),
                    'profile_pic_url' => $account->getProfilePicUrl(),
                    'profile_pic_url_hd' => $account->getProfilePicUrlHd(),
                    'biography' => $account->getBiography(),
                    'external_url' => $account->getExternalUrl(),
                    'follows_count' => $account->getFollowsCount(),
                    'followed_by_count' => $account->getFollowedByCount(),
                    'media_count' => $account->getMediaCount(),
                    'is_private' => $account->isPrivate(),
                    'is_verified' => $account->isVerified(),
                    'is_loaded' => $account->isLoaded(),
                    'created_at' => time(),
                ];
                Page_data::create($data);
                $result[$input['username']] =  ['successfully_created',$new_page->getAttributes()];
            }
            else
            {
                $result[$input['username']] =  ['this person has created recently',[]];
            }
        }
        return $this->prepare_response(200,'successfully_created',$result);

    }

    /**
     * Display the specified resource.
     *
     * @param $username_or_id
     * @return \Illuminate\Http\Response
     * @throws InstagramException
     * @throws InstagramNotFoundException
     */
    public function show($username_or_id)
    {
        $instagram = $this->get_instagram();
        $page = Page::find($username_or_id);
        $username = ($page) ? $page->username : $username_or_id;
        try{
            $account = $instagram->getAccount($username);
            $data = [
                'full_name' => $account->getFullName(),
                'profile_pic_url' => $account->getProfilePicUrl(),
                'profile_pic_url_hd' => $account->getProfilePicUrlHd(),
                'biography' => $account->getBiography(),
                'external_url' => $account->getExternalUrl(),
                'follows_count' => $account->getFollowsCount(),
                'followed_by_count' => $account->getFollowedByCount(),
                'media_count' => $account->getMediaCount(),
                'is_private' => $account->isPrivate(),
                'is_verified' => $account->isVerified(),
                'is_loaded' => $account->isLoaded(),
                'medias' => $account->getMedias()[0]
            ];
            return $this->prepare_response(200,'successful get' , $data);
        } catch (InstagramException $e) {
            return $this->prepare_response(500,$e->getMessage() , []);
        } catch (InstagramNotFoundException $e) {
            return $this->prepare_response(401,$e->getMessage() , []);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function top_followed(Request $request ,$page = '*')
    {
        $page_size = $request->input('page_size') ? $request->input('page_size') : Constants::DEFAULT_PAGE_SIZE;
        $query = Page::query();
        $query->join('page_data' , 'page_data.page_id', '=' , 'pages.id')
            ->where('page_data.created_at' , '>=' , strtotime(date('Y-m-d', time()). '00:00:00')-(3600*24))
            ->groupBy('page_data.page_id')
            ->orderBy('page_data.followed_by_count' , 'DESC');
        if($page !== '*')
        {
            $query->offset(($page-1)*$page_size)->limit($page_size);
        }
        $data = $query->get()->toArray();
        return $this->prepare_response(200,'successful get' , $data);
    }

    public function username_search(Request $request)
    {
        $searched_word = $request->input('searched_username');
        $result = Page::select(['id','username'])->where('username' , 'like' , '%'.$searched_word.'%')->get()->toArray();
//        $result = array_column( $result,'username');
        if(count($result))
        {
            return $this->prepare_response(200 , 'successful get' , $result);
        }
        else
        {
            return $this->prepare_response(200 , 'there is no username matching' , []);
        }

    }

    public function search(Request $request)
    {
        $searched_word = $request->input('searched_word');

    }

    public function page_view(Request $request)
    {
        if($request->input('username'))
        {
            page_view::create([
                'page_id' => NULL,
                'username' => $request->input('username'),
                'created_at' => date("Y-m-d H:i:s")
            ]);
            return $this->prepare_response(200,'operation_successful' , []);
        }
        elseif($request->input('page_id'))
        {
            page_view::create([
                'page_id' => $request->input('page_id'),
                'username' => NULL,
                'created_at' => date("Y-m-d H:i:s")
            ]);
            return $this->prepare_response(200,'operation_successful' , []);
        }
        else
        {
            return $this->prepare_response(403,'invalid inputs' , []);
        }


    }

    public function fill_page_id()
    {
        $instagram = $this->get_instagram();
        $pages = Page::where("instagram_id" , NULL)->get();
        foreach ($pages as $page)
        {
            try{
                $account = $instagram->getAccount($page->username);
            } catch (InstagramException $e) {
                return $this->prepare_response(500,$e->getMessage() , []);
            } catch (InstagramNotFoundException $e) {
                return $this->prepare_response(401,$e->getMessage() , []);
            }
            $record = Page::find($page->id);
            $record->instagram_id = $account->getId();
            $record->save();
        }
        return $this->prepare_response(200,'operation_done' , []);
    }

    public function interest_rates(Request $request)
    {
        $page_id = $request->input('page_id');

        $interest_rates = Interest_rate::select(['title' , 'type' , 'rate' , 'social_network_id' , 'date' , 'created_at'])->where('social_network_id' , Constants::SOCIAL_NETWORK['INSTAGRAM']['ID'])
            ->where('accountable_id' , $page_id)->groupBy(['type' , 'date'])
            ->get()->toArray();

        if($interest_rates)
        {
            return $this->prepare_response(200 , 'successful get' , $interest_rates);
        }
        else
        {
            return $this->prepare_response(204 , 'there is no data for this page' , []);
        }
    }
}
