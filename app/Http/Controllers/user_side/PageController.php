<?php

namespace App\Http\Controllers\user_side;

use App\Http\Controllers\Controller;
use App\Models\Interest_rate;
use App\Models\Page;
use App\Models\Page_data;
use App\Models\Page_media;
use Carbon\Carbon;
use Illuminate\Http\Request;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use InstagramScraper\Exception\InstagramException;
use InstagramScraper\Exception\InstagramNotFoundException;

class PageController extends Controller
{
    private $instagram;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['slider'] = true;
//        $data['top10s'] = DB::table('page_data as pd1')->leftJoin('page_data as pd2' , function($join){
//            $join->on('pd1.followed_by_count' , '=' , 'pd2.followed_by_count');
//            $join->on('pd1.id' , '=' , 'pd2.id');
//        })->whereNUll('pd2.id')->get();
        $data['top10s'] = Page::join('page_data' , 'page_data.page_id', '=' , 'pages.id')
            ->where('page_data.created_at' , '>=' , strtotime(date('Y-m-d', time()). '00:00:00')-(3600*24))
            ->groupBy('page_data.page_id')
            ->orderBy('page_data.followed_by_count' , 'DESC')
            ->limit(10)
            ->get();
        $data['favors'] = Page::with(['latest_page_data'])->limit(7)->inRandomOrder()->get();
        return view('user_side/main_page' , $data);
    }

    public function top_followed()
    {
        $data['top10s'] = Page::join('page_data' , 'page_data.page_id', '=' , 'pages.id')
            ->where('page_data.created_at' , '>=' , strtotime(date('Y-m-d', time()). '00:00:00')-(3600*24))
            ->groupBy('page_data.page_id')
            ->orderBy('page_data.followed_by_count' , 'DESC')
            ->get();
        return view('user_side/top_followed' , $data);
    }

    public function comparision()
    {
        $data = [];
        return view('user_side/comparision' , $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     * @throws GuzzleException
     */
    public function show($id)
    {
//        $curl = curl_init();
//        $url = url('api/v1/page').'/'.$id;
//        var_dump($url);
//        $url = "https://jsonplaceholder.typicode.com/posts";
//        $url = "http://localhost:8000/api/v1/page/5";
// Set some options - we are passing in a useragent too here
//        curl_setopt_array($curl, array(
//            CURLOPT_RETURNTRANSFER => 1,
//            CURLOPT_URL => $url,
//            CURLOPT_USERAGENT => 'Codular Sample cURL Request'
//        ));
//        $resp = curl_exec($curl);
//        curl_close($curl);
//        var_dump($resp);die;
//        $client = new Client();
//        $url = url('api/v1/page').'/'.$id;
//        $url = "https://jsonplaceholder.typicode.com/posts";
//        $response = $client->request('GET' , $url,['debug' => true,'connect_timeout' => 3.14,'force_ip_resolve' => 'v4' ,'verify' => false,'headers' => ['Accept'     => 'application/json']]);
//        $response = $client->get($url,['headers' => ['Accept'     => 'application/json' , 'Cache-Control' => 'no-cache','Host' => 'localhost:8000']]);
//        dd($response->getBody()->getContents());
        $data['page'] = Page::where('id' , $id)->with(['latest_page_data'])->first();

        $data['medias'] = Page_media::where('page_id' , $id)->groupBy('short_code')->orderBy('created_at' , 'DESC')->limit(8)->get();
        $data['page_id'] = $id;
        return view('user_side/page_data' , $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
