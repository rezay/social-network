<?php


namespace App\Helpers;


class Constants
{
    const DEFAULT_PAGE_SIZE = 10;
    const SOCIAL_NETWORK = [
        'INSTAGRAM' => [
            'ID' => 1 ,
            'INTEREST_RATES' => [
                'BY_COMMENTS_AND_LIKES' => [
                    'TYPE' => 1 ,
                    'TITLE' => 'interest rate by comments and likes'
                ],
                'BY_VIDEOS_VIEW' => [
                    'TYPE' => 2 ,
                    'TITLE' => 'interest rate by videos view'
                ],
            ]
        ],
        'YOUTUBE' => [
            'ID' => 2 ,
            'INTEREST_RATES' => [
                'BY_COMMENTS_AND_LIKES' => [
                    'TYPE' => 1 ,
                    'TITLE' => 'interest rate by comments and likes'
                ],
            ]
        ],
        'FACEBOOK' => [
            'ID' => 3 ,
            'INTEREST_RATES' => [
                'BY_COMMENTS_AND_LIKES' => [
                    'TYPE' => 1 ,
                    'TITLE' => 'interest rate by comments and likes'
                ],
            ]
        ],
        'TELEGRAM' => [
            'ID' => 4 ,
            'INTEREST_RATES' => [
                'BY_COMMENTS_AND_LIKES' => [
                    'TYPE' => 1 ,
                    'TITLE' => 'interest rate by comments and likes'
                ],
            ]
        ],
    ];
}
