<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    public $timestamps = true;

    public function colors()
    {
        return $this->belongsToMany(Color::Class);
    }

    public function getNameAttribute($value)
    {
        return $value.'____'.strtoupper($value);
    }

    public function setNameAttribute($value)
    {
        $this->attributes['name'] = substr(strtoupper($value) , 3 , 6);
    }
}
