<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AccountView extends Model
{
    protected $table = 'account_views';
    public $timestamps = true;
    protected $fillable = [
        'social_network_id',
        'accountable_type',
        'accountable_id',
        'total_views'
    ];
}
