<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Account_ActivityField extends Model
{
    protected $table = 'account_activity_field';
    public $timestamps = false;
    protected $fillable = [
        'social_network_id',
        'account_id',
        'activity_field_id',
    ];
}
