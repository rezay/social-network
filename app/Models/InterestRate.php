<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class InterestRate extends Model
{
    protected $table = 'interest_rates';
    public $timestamps = false;
    protected $fillable = [
        'title',
        'type',
        'rate',
        'social_network_id',
        'accountable_type',
        'accountable_id',
        'created_at',
        'timestamp',
    ];
    public function accountable()
    {
        return $this->morphTo();
    }
}
