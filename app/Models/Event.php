<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    protected $table = 'events';
    public $timestamps = true;
    protected $fillable = [
        'social_network_id',
        'accountable_type',
        'accouontable_id',
        'description',
    ];
}
