<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class InstagramAccountData extends Model
{
    protected $table = 'instagram_account_data';
    public $timestamps = false;
//    protected $attributes = [
//        'created_at' => date('Y-m-d h:i:s'),
//        'timestamp' => time()
//    ];
    protected $fillable = [
        'id',
        'instagram_account_id',
        'full_name',
        'profile_pic_url',
        'profile_pic_url_hd',
        'biography',
        'external_url',
        'follows_count',
        'followed_by_count',
        'media_count',
        'is_private',
        'is_verified',
        'is_loaded',
        'created_at',
        'timestamp',
        'date'

    ];

    public function page()
    {
        return $this->belongsTo(InstagramAccountData::class , 'id' , 'instagram_account_id');
    }
}
