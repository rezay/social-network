<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Scraping extends Model
{
    protected $table = 'scrapings';
    protected $fillable = [
        'social_network_id',
        'total_records',
        'successful_records',
        'failed_records',
        'deleted_accounts',
        'used_method',
        'date'
    ];
    public $timestamps = true;
}
