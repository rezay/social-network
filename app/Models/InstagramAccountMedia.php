<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class InstagramAccountMedia extends Model
{
    //
    protected $table = 'instagram_account_medias';
    protected $fillable = [
        'instagram_account_id' ,
        'created_time' ,
        'type' ,
        'short_code' ,
        'video_views' ,
        'likes_count' ,
        'comments_count' ,
        'caption' ,
        'image_thumbnail_url' ,
        'image_high_resolution_url' ,
        'is_caption_edited' ,
        'is_ad' ,
        'created_at',
        'timestamp',
        'date'
    ];
    public $timestamps = false;
}
