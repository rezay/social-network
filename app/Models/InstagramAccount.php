<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;

class InstagramAccount extends Model
{
    protected $table = 'instagram_accounts';
    public $timestamps = true;
    protected $fillable = [
        'username',
        'instagram_id',
    ];
    public function get_top_followed(int $offset = 0 , int $limit = NULL):object
    {
        $instagram_account_data_table = (new InstagramAccountData())->getTable();
        $query = InstagramAccount::query();
        $query->join($instagram_account_data_table, $instagram_account_data_table.'.instagram_account_id', '=' , $this->table.'.id')
            ->where($instagram_account_data_table.'.timestamp' , '>=' , strtotime(date('Y-m-d', time()). '00:00:00')-(3600*24))
            ->groupBy($instagram_account_data_table.'.instagram_account_id')
            ->orderBy($instagram_account_data_table.'.followed_by_count' , 'DESC');
        if($limit)
        {
            $query->offset($offset)->limit($limit);
        }
        return $query->get();
    }

    public function page_data()
    {
        return $this->hasMany(InstagramAccountData::Class , 'instagram_account_id' , 'id');
    }
    public function country()
    {
        return $this->hasOne(Country::Class , 'id' , 'country_id');
    }
    public function color()
    {
        return $this->hasOne(Color::Class , 'id' , 'color_id');
    }

    /**
     * @return HasOne
     */
    public function latest_page_data()
    {
        return $this->hasOne(InstagramAccountData::Class)->latest('created_at');
    }
    public function Interest_rates()
    {
        return $this->morphMany(InterestRate::Class, 'accountable');
    }
}
