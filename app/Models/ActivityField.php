<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ActivityField extends Model
{
    protected $table = 'Activity_fields';
    public $timestamps = false;
    protected $fillable = [
        'persian_name',
        'english_name',
    ];
}
