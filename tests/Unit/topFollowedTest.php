<?php

namespace Tests\Unit;

use App\Helpers\Constants;
use App\Models\Page;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Request;
use Tests\TestCase;

class topFollowedTest extends TestCase
{

    public function testTop_followed(Request $request ,$page = '*')
    {
        $page_size = $request->input('page_size') ? $request->input('page_size') : Constants::DEFAULT_PAGE_SIZE;
        $query = Page::query();
        $query->join('page_data' , 'page_data.page_id', '=' , 'pages.id')
            ->where('page_data.created_at' , '>=' , strtotime(date('Y-m-d', time()). '00:00:00')-(3600*24))
            ->groupBy('page_data.page_id')
            ->orderBy('page_data.followed_by_count' , 'DESC');
        if($page !== '*')
        {
            $query->offset(($page-1)*$page_size)->limit($page_size);
        }
        $data = $query->get()->toArray();
        $this->assertTrue(true);
        return $this->prepare_response(200,'successful get' , $data);
    }
}
