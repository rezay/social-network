<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('types')->insert(
            [
                [
                    'id' => 1,
                    'name_english' => 'نا مشخص',
                    'name_persian' => 'unknown'
                ],
                [
                    'id' => 2,
                    'name_english' => 'سیاسی',
                    'name_persian' => 'politics'
                ],
                [
                    'id' => 3,
                    'name_english' => 'ورزشی',
                    'name_persian' => 'sport'
                ],
                [
                    'id' => 4,
                    'name_english' => 'خواننده',
                    'name_persian' => 'singer'
                ],
                [
                    'id' => 5,
                    'name_english' => 'سلبریتی',
                    'name_persian' => 'celebrity'
                ],
                [
                    'id' => 6,
                    'name_english' => 'بازیگر',
                    'name_persian' => 'actor/actress'
                ],
                [
                    'id' => 7,
                    'name_english' => 'شرکت',
                    'name_persian' => 'company'
                ],
                [
                    'id' => 8,
                    'name_english' => 'سرگرمی',
                    'name_persian' => 'fun'
                ],
                [
                    'id' => 9,
                    'name_english' => 'مجری',
                    'name_persian' => 'executor'
                ],
                [
                    'id' => 10,
                    'name_english' => 'اموزشی',
                    'name_persian' => 'learning'
                ],
                [
                    'id' => 11,
                    'name_english' => 'رییس جمهور',
                    'name_persian' => 'president'
                ],
                [
                    'id' => 12,
                    'name_english' => 'گیمر',
                    'name_persian' => 'gamer'
                ],

            ]
            );
    }
}
