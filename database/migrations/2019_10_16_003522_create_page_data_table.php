<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePageDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('page_data', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('page_id');
            $table->string('full_name')->nullable();
            $table->string('profile_pic_url',2048)->nullable();
            $table->string('profile_pic_url_hd' , 2048)->nullable();
            $table->string('biography' , 4096)->nullable();
            $table->string('external_url' , 2048)->nullable();
            $table->integer('follows_count')->default(0);
            $table->integer('followed_by_count')->default(0);
            $table->integer('media_count')->default(0);
            $table->boolean('is_private');
            $table->boolean('is_verified');
            $table->boolean('is_loaded');
            $table->integer('created_at');
            $table->index(['page_id']);
            $table->engine = "INNODB";
            $table->charset = 'utf8mb4';
            $table->collation = 'utf8mb4_general_ci';
//            $table->foreign('page_id')->references('id')->on('pages');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('page_data');
    }
}
