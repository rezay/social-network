<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::prefix('/instagram')->group(function(){
    Route::prefix('/')->group(function(){
        Route::get('/' , 'user_side\PageController@index');
        Route::get('/page/top-followed' , 'user_side\PageController@top_followed');
        Route::get('/page/comparision' , 'user_side\PageController@comparision');
        Route::resource('/page' , 'user_side\PageController');
    });
    Route::prefix('/admin-dashboard')->group(function(){
        Route::resource('page' , 'admin_dashboard\PageController');
    });
});



