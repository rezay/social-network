<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});

Route::prefix('/v1')->group(function(){
    Route::prefix('/instagram')->group(function(){
        Route::get('/test', 'Controller@test');
        Route::prefix('/page')->group(function(){
            Route::patch('/instagram-id', 'api\PageController@fill_page_id');
            Route::get('/search', 'api\PageController@search');
            Route::get('/top-followed/{page?}', 'api\PageController@top_followed');
            Route::get('/username-search', 'api\PageController@username_search');
            Route::get('/interest-rates', 'api\PageController@interest_rates');
            Route::post('/view', 'api\PageController@page_view');
        });
        Route::prefix('/page-data')->group(function(){
            Route::post('/store-new-data', 'api\PageDataController@store_new_data');
            Route::delete('/duplicates', 'api\PageDataController@remove_duplicates');
        });

        Route::apiResources(
            [
                'page' => 'api\PageController',
                'page-data' => 'api\PageDataController'
            ]);
    });
});

