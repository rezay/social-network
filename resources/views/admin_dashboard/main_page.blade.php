@extends('admin_dashboard/layout/admin_dashboard_layout')
@section('title')
    <title>صفحه اصلی</title>
@endsection
@section('styles')

@endsection
@section('contents')
    <div id="wrapper">
        <nav class="navbar-default navbar-static-side" role="navigation">
            <div class="sidebar-collapse">
                <ul class="nav metismenu" id="side-menu">
                    <li class="nav-header">
                        <div class="dropdown profile-element"> <span>
                                <img alt="image" class="img-circle" src="img/profile_small.jpg" />
                                 </span>
                            <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                                <span class="clear"> <span class="block m-t-xs"> <strong class="font-bold">ایمان عباسی</strong>
                                 </span> <span class="text-muted text-xs block">مدیر هنری <b class="caret"></b></span> </span> </a>
                            <ul class="dropdown-menu animated fadeInLeft m-t-xs">
                                <li><a href="profile.html">پروفایل</a></li>
                                <li><a href="contacts.html">مخاطبین</a></li>
                                <li><a href="mailbox.html">ایمیل</a></li>
                                <li class="divider"></li>
                                <li><a href="login.html">خروج</a></li>
                            </ul>
                        </div>
                        <div class="logo-element">
                            لوگو
                        </div>
                    </li>
                    <li class="active">
                        <a href="index-2.html"><i class="fa fa-th-large"></i> <span class="nav-label">داشبورد ها</span> <span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level collapse">
                            <li class="active"><a href="index-2.html">داشبورد ورژن 1</a></li>
                            <li><a href="dashboard_2.html">داشبورد ورژن 2</a></li>
                            <li><a href="dashboard_3.html">داشبورد ورژن 3</a></li>
                            <li><a href="dashboard_4_1.html">داشبورد ورژن 4</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="layouts.html"><i class="fa fa-diamond"></i> <span class="nav-label">طرح ها</span></a>
                    </li>
                    <li>
                        <a href="mailbox.html"><i class="fa fa-envelope"></i> <span class="nav-label">ایمیل </span><span class="label label-warning pull-left">16/24</span></a>
                        <ul class="nav nav-second-level collapse">
                            <li><a href="mailbox.html">دریافت</a></li>
                            <li><a href="mail_detail.html">مشاهده ایمیل</a></li>
                            <li><a href="mail_compose.html">ارسال ایمیل</a></li>
                            <li><a href="email_template.html">قالب ایمیل</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="metrics.html"><i class="fa fa-pie-chart"></i> <span class="nav-label">معیارها </span> <span class="label label-primary pull-left">جدید</span> </a>
                    </li>
                    <li>
                        <a href="widgets.html"><i class="fa fa-flask"></i> <span class="nav-label">ویجت ها</span></a>
                    </li>
                    <li>
                        <a href="css_animation.html"><i class="fa fa-magic"></i> <span class="nav-label">انیمیشن ها </span><span class="label label-info pull-left">62</span></a>
                    </li>
                    <li class="landing_link">
                        <a target="_blank" href="landing.html"><i class="fa fa-star"></i> <span class="nav-label">صفحه فرود</span> <span class="label label-warning pull-left">جدید</span></a>
                    </li>
                    <li class="special_link">
                        <a href="package.html"><i class="fa fa-database"></i> <span class="nav-label">پکیج ها</span></a>
                    </li>
                </ul>
            </div>
        </nav>
        <div id="page-wrapper" class="gray-bg dashbard-1">
            <div class="row border-bottom">
                <nav class="navbar navbar-static-top" role="navigation">
                    <div class="navbar-header">
                        <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a>
                        <form role="search" class="navbar-form-custom" action="search_results.html">
                            <div class="form-group">
                                <input type="text" placeholder="جستجو" class="form-control" name="top-search" id="top-search">
                            </div>
                        </form>
                    </div>
                    <ul class="nav navbar-top-links navbar-left">
                        <li>
                            <span class="m-r-sm text-muted welcome-message">به بخش مدیریت خوش آمدید</span>
                        </li>
                        <li class="dropdown">
                            <a class="dropdown-toggle count-info" data-toggle="dropdown" href="#">
                                <i class="fa fa-envelope"></i>  <span class="label label-warning">16</span>
                            </a>
                            <ul class="dropdown-menu dropdown-messages">
                                <li>
                                    <div class="dropdown-messages-box">
                                        <a href="profile.html" class="pull-right">
                                            <img alt="image" class="img-circle" src="img/a4.jpg">
                                        </a>
                                        <div class="media-body ">
                                            <small class="pull-left text-navy">5 ساعت پیش</small>
                                            <strong>ایمن</strong> لورم ایپسوم <strong>ایمن</strong>. <br>
                                            <small class="text-muted">دیروز 1:21 ب.ظ - 1394/06/10</small>
                                        </div>
                                    </div>
                                </li>
                                <li class="divider"></li>
                                <li>
                                    <div class="dropdown-messages-box">
                                        <a href="profile.html" class="pull-right">
                                            <img alt="image" class="img-circle" src="img/a7.jpg">
                                        </a>
                                        <div class="media-body">
                                            <small class="pull-left">46 ساعت پیش</small>
                                            <strong>ایمان عباسی</strong> لورم ایپسوم <strong>ایمن</strong>. <br>
                                            <small class="text-muted">3 روز پیش در 7:58 ب.ظ - 1394/06/10</small>
                                        </div>
                                    </div>
                                </li>
                                <li class="divider"></li>
                                <li>
                                    <div class="dropdown-messages-box">
                                        <a href="profile.html" class="pull-right">
                                            <img alt="image" class="img-circle" src="img/profile.jpg">
                                        </a>
                                        <div class="media-body ">
                                            <small class="pull-left">23 ساعت پیش</small>
                                            <strong>ایمان عباسی</strong> لورم ایپسوم <strong>ایمن</strong>. <br>
                                            <small class="text-muted">2 وز پیش در 2:30 ق.ظ - 1394/06/10</small>
                                        </div>
                                    </div>
                                </li>
                                <li class="divider"></li>
                                <li>
                                    <div class="text-center link-block">
                                        <a href="mailbox.html">
                                            <i class="fa fa-envelope"></i> <strong>مشاهده همه پیغام ها</strong>
                                        </a>
                                    </div>
                                </li>
                            </ul>
                        </li>
                        <li class="dropdown">
                            <a class="dropdown-toggle count-info" data-toggle="dropdown" href="#">
                                <i class="fa fa-bell"></i>  <span class="label label-primary">8</span>
                            </a>
                            <ul class="dropdown-menu dropdown-alerts">
                                <li>
                                    <a href="mailbox.html">
                                        <div>
                                            <i class="fa fa-envelope fa-fw"></i> شما 16 پیغام دارید
                                            <span class="pull-left text-muted small">4 دقیقه پیش</span>
                                        </div>
                                    </a>
                                </li>
                                <li class="divider"></li>
                                <li>
                                    <a href="profile.html">
                                        <div>
                                            <i class="fa fa-twitter fa-fw"></i> 3 فالوور جدید
                                            <span class="pull-left text-muted small">12 دقیقه پیش</span>
                                        </div>
                                    </a>
                                </li>
                                <li class="divider"></li>
                                <li>
                                    <a href="grid_options.html">
                                        <div>
                                            <i class="fa fa-upload fa-fw"></i> سرور ری استارت شد
                                            <span class="pull-left text-muted small">4 دقیقه پیش</span>
                                        </div>
                                    </a>
                                </li>
                                <li class="divider"></li>
                                <li>
                                    <div class="text-center link-block">
                                        <a href="notifications.html">
                                            <strong>مشاهده همه هشدار ها</strong>
                                            <i class="fa fa-angle-left fa-lg fa-fw"></i>
                                        </a>
                                    </div>
                                </li>
                            </ul>
                        </li>


                        <li>
                            <a href="login.html">
                                <i class="fa fa-sign-out"></i> خروج
                            </a>
                        </li>
                        <li>
                            <a class="left-sidebar-toggle">
                                <i class="fa fa-tasks"></i>
                            </a>
                        </li>
                    </ul>
                </nav>
            </div>
            <div class="row border-bottom white-bg dashboard-header">
                <div class="col-sm-3">
                    <h2>خوش آمدید ایمان</h2>
                    <small>شما 42 پیغام 6 نوتیفیکیشن دارید.</small>
                    <ul class="list-group clear-list m-t">
                        <li class="list-group-item fist-item">
                                        <span class="pull-left">
                                            09:00 ب.ظ
                                        </span>
                            <span class="label label-success">1</span> لطفا با من تماس بگیرید
                        </li>
                        <li class="list-group-item">
                                        <span class="pull-left">
                                            10:16 ق.ظ
                                        </span>
                            <span class="label label-info">2</span> امضای قرارداد
                        </li>
                        <li class="list-group-item">
                                        <span class="pull-left">
                                            08:22 ب.ظ
                                        </span>
                            <span class="label label-primary">3</span> افتتاح شعبه جدید
                        </li>
                        <li class="list-group-item">
                                        <span class="pull-left">
                                            11:06 ب.ظ
                                        </span>
                            <span class="label label-default">4</span> تماس با ایمن
                        </li>
                        <li class="list-group-item">
                                        <span class="pull-left">
                                            12:00 ق.ظ
                                        </span>
                            <span class="label label-primary">5</span> نوشتن نامه به ایمن
                        </li>
                    </ul>
                </div>
                <div class="col-sm-6">
                    <div class="flot-chart dashboard-chart">
                        <div class="flot-chart-content" id="flot-dashboard-chart"></div>
                    </div>
                    <div class="row text-left">
                        <div class="col-xs-4">
                            <div class=" m-l-md">
                                <span class="h4 font-bold m-t block">406,100 تومان</span>
                                <small class="text-muted m-b block">گزارش بازاریابی فروش</small>
                            </div>
                        </div>
                        <div class="col-xs-4">
                            <span class="h4 font-bold m-t block">150,401 تومان</span>
                            <small class="text-muted m-b block">درآمد فروش سالانه</small>
                        </div>
                        <div class="col-xs-4">
                            <span class="h4 font-bold m-t block">16,822 تومان</span>
                            <small class="text-muted m-b block">درآمد حاشیه ای نیم سال</small>
                        </div>

                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="statistic-box">
                        <h4>
                            پیشرفت پروژه بتا
                        </h4>
                        <p>
                            شما دو پروژه با وظایف تکمیل نشده دارید.
                        </p>
                        <div class="row text-center">
                            <div class="col-lg-6">
                                <canvas id="polarChart" width="80" height="80"></canvas>
                                <h5 >کولتر</h5>
                            </div>
                            <div class="col-lg-6">
                                <canvas id="doughnutChart" width="78" height="78"></canvas>
                                <h5 >مکستور</h5>
                            </div>
                        </div>
                        <div class="m-t">
                            <small>لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است.</small>
                        </div>

                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="wrapper wrapper-content">
                        <div class="row">
                            <div class="col-lg-4">
                                <div class="ibox float-e-margins">
                                    <div class="ibox-title">
                                        <h5>اطلاعات جدید جهت گزارش گیری</h5> <span class="label label-primary">برچسب</span>
                                        <div class="ibox-tools">
                                            <a class="collapse-link">
                                                <i class="fa fa-chevron-up"></i>
                                            </a>
                                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                                <i class="fa fa-wrench"></i>
                                            </a>
                                            <ul class="dropdown-menu dropdown-user">
                                                <li><a href="#">گزینه 1</a>
                                                </li>
                                                <li><a href="#">گزینه 2</a>
                                                </li>
                                            </ul>
                                            <a class="close-link">
                                                <i class="fa fa-times"></i>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="ibox-content">
                                        <div>

                                            <div class="pull-left text-left">

                                                <span class="bar_dashboard">5,3,9,6,5,9,7,3,5,2,4,7,3,2,7,9,6,4,5,7,3,2,1,0,9,5,6,8,3,2,1</span>
                                                <br/>
                                                <small class="font-bold">20054.43 اتومان</small>
                                            </div>
                                            <h4>اطلاعات جدید !
                                                <br/>
                                                <small class="m-l"><a href="graph_flot.html"> قیمت سهام را بررسی نمایید! </a> </small>
                                            </h4>
                                        </div>
                                    </div>
                                </div>
                                <div class="ibox float-e-margins">
                                    <div class="ibox-title">
                                        <h5>نظرات زیر را بخوانید</h5>
                                        <div class="ibox-tools">
                                            <a class="collapse-link">
                                                <i class="fa fa-chevron-up"></i>
                                            </a>
                                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                                <i class="fa fa-wrench"></i>
                                            </a>
                                            <ul class="dropdown-menu dropdown-user">
                                                <li><a href="#">گزینه 1</a>
                                                </li>
                                                <li><a href="#">گزینه 2</a>
                                                </li>
                                            </ul>
                                            <a class="close-link">
                                                <i class="fa fa-times"></i>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="ibox-content no-padding">
                                        <ul class="list-group">
                                            <li class="list-group-item">
                                                <p><a class="text-info" href="#">@لورم</a> ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است.</p>
                                                <small class="block text-muted"><i class="fa fa-clock-o"></i> 1 دقیقه پیش</small>
                                            </li>
                                            <li class="list-group-item">
                                                <p><a class="text-info" href="#">@لورم</a> ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. </p>
                                                <div class="text-center m">
                                                    <span id="sparkline8"></span>
                                                </div>
                                                <small class="block text-muted"><i class="fa fa-clock-o"></i> 2 ساعت پیش</small>
                                            </li>
                                            <li class="list-group-item">
                                                <p><a class="text-info" href="#">@لورم</a> ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. </p>
                                                <small class="block text-muted"><i class="fa fa-clock-o"></i> 2 دقیقه پیش</small>
                                            </li>
                                            <li class="list-group-item ">
                                                <p><a class="text-info" href="#">@لورم</a> ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است.</p>
                                                <small class="block text-muted"><i class="fa fa-clock-o"></i> 1 ساعت پیش</small>
                                            </li>
                                            <li class="list-group-item">
                                                <p><a class="text-info" href="#">@لورم</a> ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. </p>
                                                <small class="block text-muted"><i class="fa fa-clock-o"></i> 2 دقیقه پیش</small>
                                            </li>
                                            <li class="list-group-item ">
                                                <p><a class="text-info" href="#">@لورم</a> ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است.</p>
                                                <small class="block text-muted"><i class="fa fa-clock-o"></i> 1 ساعت پیش</small>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="ibox float-e-margins">
                                    <div class="ibox-title">
                                        <h5>خبرهای روزانه</h5>
                                        <div class="ibox-tools">
                                            <span class="label label-warning-light">10 پیغام</span>
                                        </div>
                                    </div>
                                    <div class="ibox-content">

                                        <div>
                                            <div class="feed-activity-list">

                                                <div class="feed-element">
                                                    <a href="profile.html" class="pull-right m-l-xs">
                                                        <img alt="image" class="img-circle" src="img/profile.jpg">
                                                    </a>
                                                    <div class="media-body">
                                                        <small class="pull-left">5 دقیقه پیش</small>
                                                        <strong>ایمن</strong> پستی در بلاگ اضافه کرد <br>
                                                        <small class="text-muted">امروز 5:60 ب.ظ - 1394.06.11</small>

                                                    </div>
                                                </div>
                                                <div class="feed-element">
                                                    <a href="profile.html" class="pull-right m-l-xs">
                                                        <img alt="image" class="img-circle" src="img/a2.jpg">
                                                    </a>
                                                    <div class="media-body ">
                                                        <small class="pull-left">2 ساعت پیش</small>
                                                        <strong>ایمن</strong> پیغامی به <strong>ایمن</strong> فرستاد. <br>
                                                        <small class="text-muted">امروز 2:10 ب.ظ - 1394.06.12</small>
                                                    </div>
                                                </div>
                                                <div class="feed-element">
                                                    <a href="profile.html" class="pull-right m-l-xs">
                                                        <img alt="image" class="img-circle" src="img/profile.jpg">
                                                    </a>
                                                    <div class="media-body ">
                                                        <small class="pull-left">5 دقیقه پیش</small>
                                                        <strong>ایمن</strong> پستی در بلاگ اضافه کرد <br>
                                                        <small class="text-muted">امروز 5:60 ب.ظ - 1394.06.11</small>

                                                    </div>
                                                </div>
                                                <div class="feed-element">
                                                    <a href="profile.html" class="pull-right m-l-xs">
                                                        <img alt="image" class="img-circle" src="img/a2.jpg">
                                                    </a>
                                                    <div class="media-body ">
                                                        <small class="pull-left">2 ساعت پیش</small>
                                                        <strong>ایمن</strong> پیغامی به <strong>ایمن</strong> فرستاد. <br>
                                                        <small class="text-muted">امروز 2:10 ب.ظ - 1394.06.12</small>
                                                    </div>
                                                </div>
                                                <div class="feed-element">
                                                    <a href="profile.html" class="pull-right m-l-xs">
                                                        <img alt="image" class="img-circle" src="img/profile.jpg">
                                                    </a>
                                                    <div class="media-body ">
                                                        <small class="pull-left">5 دقیقه پیش</small>
                                                        <strong>ایمن</strong> پستی در بلاگ اضافه کرد <br>
                                                        <small class="text-muted">امروز 5:60 ب.ظ - 1394.06.11</small>

                                                    </div>
                                                </div>
                                                <div class="feed-element">
                                                    <a href="profile.html" class="pull-right m-l-xs">
                                                        <img alt="image" class="img-circle" src="img/a2.jpg">
                                                    </a>
                                                    <div class="media-body ">
                                                        <small class="pull-left">2 ساعت پیش</small>
                                                        <strong>ایمن</strong> پیغامی به <strong>ایمن</strong> فرستاد. <br>
                                                        <small class="text-muted">امروز 2:10 ب.ظ - 1394.06.12</small>
                                                    </div>
                                                </div>
                                                <div class="feed-element">
                                                    <a href="profile.html" class="pull-right m-l-xs">
                                                        <img alt="image" class="img-circle" src="img/profile.jpg">
                                                    </a>
                                                    <div class="media-body ">
                                                        <small class="pull-left">5 دقیقه پیش</small>
                                                        <strong>ایمن</strong> پستی در بلاگ اضافه کرد <br>
                                                        <small class="text-muted">امروز 5:60 ب.ظ - 1394.06.11</small>

                                                    </div>
                                                </div>
                                            </div>

                                            <button class="btn btn-primary btn-block m-t"><i class="fa fa-arrow-down"></i> نمایش بیشتر</button>

                                        </div>

                                    </div>
                                </div>

                            </div>
                            <div class="col-lg-4">
                                <div class="ibox float-e-margins">
                                    <div class="ibox-title">
                                        <h5>پروژه آلفا</h5>
                                        <div class="ibox-tools">
                                            <a class="collapse-link">
                                                <i class="fa fa-chevron-up"></i>
                                            </a>
                                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                                <i class="fa fa-wrench"></i>
                                            </a>
                                            <ul class="dropdown-menu dropdown-user">
                                                <li><a href="#">گزینه 1</a>
                                                </li>
                                                <li><a href="#">گزینه 2</a>
                                                </li>
                                            </ul>
                                            <a class="close-link">
                                                <i class="fa fa-times"></i>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="ibox-content ibox-heading">
                                        <h3>شما امروز قرار ملاقات دارید!</h3>
                                        <small><i class="fa fa-map-marker"></i> قرار شما برای ساعت 6 ب.ظ برنامه ریزی شده است.</small>
                                    </div>
                                    <div class="ibox-content inspinia-timeline">

                                        <div class="timeline-item">
                                            <div class="row">
                                                <div class="col-xs-3 date">
                                                    <i class="fa fa-briefcase"></i>
                                                    6:00 ق.ظ
                                                    <br/>
                                                    <small class="text-navy">2 ساعت پیش</small>
                                                </div>
                                                <div class="col-xs-7 content no-top-border">
                                                    <p class="m-b-xs"><strong>جلسه</strong></p>

                                                    <p>لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است و برای شرایط فعلی تکنولوژی مورد نیاز و کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می باشد.</p>

                                                    <p><span data-diameter="40" class="updating-chart">5,3,9,6,5,9,7,3,5,2,5,3,9,6,5,9,4,7,3,2,9,8,7,4,5,1,2,9,5,4,7,2,7,7,3,5,2</span></p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="timeline-item">
                                            <div class="row">
                                                <div class="col-xs-3 date">
                                                    <i class="fa fa-file-text"></i>
                                                    7:00 ق.ظ
                                                    <br/>
                                                    <small class="text-navy">3 ساعت پیش</small>
                                                </div>
                                                <div class="col-xs-7 content">
                                                    <p class="m-b-xs"><strong>ارسال مدارک به ایمن</strong></p>
                                                    <p>لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است و برای شرایط فعلی تکنولوژی مورد نیاز و کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می باشد.</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="timeline-item">
                                            <div class="row">
                                                <div class="col-xs-3 date">
                                                    <i class="fa fa-coffee"></i>
                                                    8:00 ق.ظ
                                                    <br/>
                                                </div>
                                                <div class="col-xs-7 content">
                                                    <p class="m-b-xs"><strong>وقت استراحت</strong></p>
                                                    <p>
                                                        لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است و برای شرایط فعلی تکنولوژی مورد نیاز و کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می باشد.
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="timeline-item">
                                            <div class="row">
                                                <div class="col-xs-3 date">
                                                    <i class="fa fa-phone"></i>
                                                    11:00 ق.ظ
                                                    <br/>
                                                    <small class="text-navy">21 ساعت پیش</small>
                                                </div>
                                                <div class="col-xs-7 content">
                                                    <p class="m-b-xs"><strong>تماس با ایمن</strong></p>
                                                    <p>
                                                        لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است.
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="timeline-item">
                                            <div class="row">
                                                <div class="col-xs-3 date">
                                                    <i class="fa fa-user-md"></i>
                                                    09:00 ب.ظ
                                                    <br/>
                                                    <small>21 ساعت پیش</small>
                                                </div>
                                                <div class="col-xs-7 content">
                                                    <p class="m-b-xs"><strong>تماس با ایمن</strong></p>
                                                    <p>
                                                        لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است و برای شرایط فعلی تکنولوژی مورد نیاز و کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می باشد.
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="timeline-item">
                                            <div class="row">
                                                <div class="col-xs-3 date">
                                                    <i class="fa fa-comments"></i>
                                                    12:50 ب.ظ
                                                    <br/>
                                                    <small class="text-navy">48 ساعت پیش</small>
                                                </div>
                                                <div class="col-xs-7 content">
                                                    <p class="m-b-xs"><strong>گفتگو با ایمن</strong></p>
                                                    <p>
                                                        لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است و برای شرایط فعلی تکنولوژی مورد نیاز و کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می باشد.
                                                    </p>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="footer">
                        <div class="pull-left">
                            10گیگا بایت از<strong>250 گیگابایت</strong> خالی است.
                        </div>
                        <div>
                            <strong>
                                کلیه حقوق محفوظ است</strong> Webbyme &copy; 1393-1394
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="small-chat-box fadeInRight animated">

            <div class="heading" draggable="true">
                <small class="chat-date pull-left">
                    1394.10.20
                </small>
                گفتگو زنده
            </div>

            <div class="content">

                <div class="left">
                    <div class="author-name">
                        ایمن <small class="chat-date">
                            10:02 ق.ظ
                        </small>
                    </div>
                    <div class="chat-message active">
                        لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده.
                    </div>

                </div>
                <div class="right">
                    <div class="author-name">
                        نیک
                        <small class="chat-date">
                            11:24 ق.ظ
                        </small>
                    </div>
                    <div class="chat-message">
                        لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم.
                    </div>
                </div>
                <div class="left">
                    <div class="author-name">
                        آلیس
                        <small class="chat-date">
                            08:45 ب.ظ
                        </small>
                    </div>
                    <div class="chat-message active">
                        لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده.
                    </div>
                </div>
                <div class="right">
                    <div class="author-name">
                        آنا
                        <small class="chat-date">
                            11:24 ق.ظ
                        </small>
                    </div>
                    <div class="chat-message">
                        لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده
                    </div>
                </div>
                <div class="left">
                    <div class="author-name">
                        نیک
                        <small class="chat-date">
                            08:45 ب.ظ
                        </small>
                    </div>
                    <div class="chat-message active">
                        لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده.
                    </div>
                </div>


            </div>
            <div class="form-chat">
                <div class="input-group input-group-sm"><input type="text" class="form-control"> <span class="input-group-btn"> <button
                                class="btn btn-primary" type="button">ارسال
                    </button> </span></div>
            </div>
        </div>
        <div id="small-chat">
            <span class="badge badge-warning pull-right">5</span>
            <a class="open-small-chat">
                <i class="fa fa-comments"></i>
            </a>
        </div>
        <div id="left-sidebar">
            <div class="sidebar-container">
                <ul class="nav nav-tabs navs-3">
                    <li class="active"><a data-toggle="tab" href="#tab-1">
                            یادداشت ها
                        </a></li>
                    <li><a data-toggle="tab" href="#tab-2">
                            پروژه ها
                        </a></li>
                    <li class=""><a data-toggle="tab" href="#tab-3">
                            <i class="fa fa-gear"></i>
                        </a></li>
                </ul>
                <div class="tab-content">
                    <div id="tab-1" class="tab-pane active">
                        <div class="sidebar-title">
                            <h3> <i class="fa fa-comments-o"></i> آخرین یادداشت ها</h3>
                            <small><i class="fa fa-tim"></i> شما 10 پیغام جدید دارید.</small>
                        </div>
                        <div>
                            <div class="sidebar-message">
                                <a href="#">
                                    <div class="pull-right text-center">
                                        <img alt="image" class="img-circle message-avatar" src="img/a1.jpg">

                                        <div class="m-t-xs">
                                            <i class="fa fa-star text-warning"></i>
                                            <i class="fa fa-star text-warning"></i>
                                        </div>
                                    </div>
                                    <div class="media-body">

                                        لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده.
                                        <br>
                                        <small class="text-muted">امروز 4:21 ب.ظ</small>
                                    </div>
                                </a>
                            </div>
                            <div class="sidebar-message">
                                <a href="#">
                                    <div class="pull-right text-center">
                                        <img alt="image" class="img-circle message-avatar" src="img/a2.jpg">
                                    </div>
                                    <div class="media-body">
                                        لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده.
                                        <br>
                                        <small class="text-muted">دیروز 2:45 ب.ظ</small>
                                    </div>
                                </a>
                            </div>
                            <div class="sidebar-message">
                                <a href="#">
                                    <div class="pull-right text-center">
                                        <img alt="image" class="img-circle message-avatar" src="img/a3.jpg">

                                        <div class="m-t-xs">
                                            <i class="fa fa-star text-warning"></i>
                                            <i class="fa fa-star text-warning"></i>
                                            <i class="fa fa-star text-warning"></i>
                                        </div>
                                    </div>
                                    <div class="media-body">
                                        لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده.
                                        <br>
                                        <small class="text-muted">دیروز 1:10 ب.ظ</small>
                                    </div>
                                </a>
                            </div>
                            <div class="sidebar-message">
                                <a href="#">
                                    <div class="pull-right text-center">
                                        <img alt="image" class="img-circle message-avatar" src="img/a4.jpg">
                                    </div>

                                    <div class="media-body">
                                        لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده.
                                        <br>
                                        <small class="text-muted">دوشنبه 8:37 ب.ظ</small>
                                    </div>
                                </a>
                            </div>
                            <div class="sidebar-message">
                                <a href="#">
                                    <div class="pull-right text-center">
                                        <img alt="image" class="img-circle message-avatar" src="img/a8.jpg">
                                    </div>
                                    <div class="media-body">
                                        لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده.
                                        <br>
                                        <small class="text-muted">امروز 4:21 ب.ظ</small>
                                    </div>
                                </a>
                            </div>
                            <div class="sidebar-message">
                                <a href="#">
                                    <div class="pull-right text-center">
                                        <img alt="image" class="img-circle message-avatar" src="img/a7.jpg">
                                    </div>
                                    <div class="media-body">
                                        لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده.
                                        <br>
                                        <small class="text-muted">دیروز 2:45 ب.ظ</small>
                                    </div>
                                </a>
                            </div>
                            <div class="sidebar-message">
                                <a href="#">
                                    <div class="pull-right text-center">
                                        <img alt="image" class="img-circle message-avatar" src="img/a3.jpg">

                                        <div class="m-t-xs">
                                            <i class="fa fa-star text-warning"></i>
                                            <i class="fa fa-star text-warning"></i>
                                            <i class="fa fa-star text-warning"></i>
                                        </div>
                                    </div>
                                    <div class="media-body">
                                        لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده.
                                        <br>
                                        <small class="text-muted">دیروز 1:10 ب.ظ</small>
                                    </div>
                                </a>
                            </div>
                            <div class="sidebar-message">
                                <a href="#">
                                    <div class="pull-right text-center">
                                        <img alt="image" class="img-circle message-avatar" src="img/a4.jpg">
                                    </div>
                                    <div class="media-body">
                                        لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده.
                                        <br>
                                        <small class="text-muted">دوشنبه 8:37 ب.ظ</small>
                                    </div>
                                </a>
                            </div>
                        </div>

                    </div>

                    <div id="tab-2" class="tab-pane">

                        <div class="sidebar-title">
                            <h3> <i class="fa fa-cube"></i> آخریم پروژه ها</h3>
                            <small><i class="fa fa-tim"></i> شما 14 پروژه دارید که 10 تای آن نا تمام است.</small>
                        </div>

                        <ul class="sidebar-list">
                            <li>
                                <a href="#">
                                    <div class="small pull-left m-t-xs">9 ساعت پیش</div>
                                    <h4>پروژه ی لورم</h4>
                                    لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده.
                                    <div class="small">درصد پیشرفت: 22%</div>
                                    <div class="progress progress-mini">
                                        <div style="width: 22%;" class="progress-bar progress-bar-warning"></div>
                                    </div>
                                    <div class="small text-muted m-t-xs">تاریخ پایان: 4:00 ب.ظ - 1394/06/10</div>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <div class="small pull-left m-t-xs">9 ساعت پیش</div>
                                    <h4>قرارداد با شرکت </h4>
                                    لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است.
                                    <div class="small">درصد پیشرفت: 48%</div>
                                    <div class="progress progress-mini">
                                        <div style="width: 48%;" class="progress-bar"></div>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <div class="small pull-left m-t-xs">9 ساعت پیش</div>
                                    <h4>جلسه</h4>
                                    لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است.
                                    <div class="small">درصد پیشرفت: 14%</div>
                                    <div class="progress progress-mini">
                                        <div style="width: 14%;" class="progress-bar progress-bar-info"></div>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <span class="label label-primary pull-left">جدید</span>
                                    <h4>تولید</h4>
                                    <!--<div class="small pull-right m-t-xs">9 ساعت پیش</div>-->
                                    لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است.
                                    <div class="small">درصد پیشرفت: 22%</div>
                                    <div class="small text-muted m-t-xs">تاریخ پایان: 4:00 ب.ظ - 1394/05/31</div>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <div class="small pull-left m-t-xs">9 ساعت پیش</div>
                                    <h4>پروژه</h4>
                                    لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است.
                                    <div class="small">درصد پیشرفت: 22%</div>
                                    <div class="progress progress-mini">
                                        <div style="width: 22%;" class="progress-bar progress-bar-warning"></div>
                                    </div>
                                    <div class="small text-muted m-t-xs">تاریخ پایان: 4:00 ب.ظ - 1394/02/14</div>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <div class="small pull-left m-t-xs">9 ساعت پیش</div>
                                    <h4>قرارداد مجدد </h4>
                                    لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است.
                                    <div class="small">درصد پیشرفت: 48%</div>
                                    <div class="progress progress-mini">
                                        <div style="width: 48%;" class="progress-bar"></div>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <div class="small pull-left m-t-xs">9 ساعت پیش</div>
                                    <h4>جلسه</h4>
                                    لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است.
                                    <div class="small">درصد پیشرفت: 14%</div>
                                    <div class="progress progress-mini">
                                        <div style="width: 14%;" class="progress-bar progress-bar-info"></div>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <span class="label label-primary pull-left">جدید</span>
                                    <h4>تولید</h4>
                                    <!--<div class="small pull-right m-t-xs">9 ساعت پیش</div>-->
                                    لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است.
                                    <div class="small">درصد پیشرفت: 22%</div>
                                    <div class="small text-muted m-t-xs">تاریخ پایان: 4:00 ب.ظ - 1394/06/14</div>
                                </a>
                            </li>

                        </ul>

                    </div>

                    <div id="tab-3" class="tab-pane">

                        <div class="sidebar-title">
                            <h3><i class="fa fa-gears"></i> تنظیمات</h3>
                            <small><i class="fa fa-tim"></i>  شما 14 پروژه دارید که 10 تای آن نا تمام است.</small>
                        </div>

                        <div class="setings-item">
                        <span>
                            نمایش اطلاع رسانی
                        </span>
                            <div class="switch">
                                <div class="onoffswitch">
                                    <input type="checkbox" name="collapsemenu" class="onoffswitch-checkbox" id="example">
                                    <label class="onoffswitch-label" for="example">
                                        <span class="onoffswitch-inner"></span>
                                        <span class="onoffswitch-switch"></span>
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="setings-item">
                        <span>
                            غیرفعال کردن گفتگو
                        </span>
                            <div class="switch">
                                <div class="onoffswitch">
                                    <input type="checkbox" name="collapsemenu" checked class="onoffswitch-checkbox" id="example2">
                                    <label class="onoffswitch-label" for="example2">
                                        <span class="onoffswitch-inner"></span>
                                        <span class="onoffswitch-switch"></span>
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="setings-item">
                        <span>
                            فعال سازی تاریخچه
                        </span>
                            <div class="switch">
                                <div class="onoffswitch">
                                    <input type="checkbox" name="collapsemenu" class="onoffswitch-checkbox" id="example3">
                                    <label class="onoffswitch-label" for="example3">
                                        <span class="onoffswitch-inner"></span>
                                        <span class="onoffswitch-switch"></span>
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="setings-item">
                        <span>
                            نمایش چارت ها
                        </span>
                            <div class="switch">
                                <div class="onoffswitch">
                                    <input type="checkbox" name="collapsemenu" class="onoffswitch-checkbox" id="example4">
                                    <label class="onoffswitch-label" for="example4">
                                        <span class="onoffswitch-inner"></span>
                                        <span class="onoffswitch-switch"></span>
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="setings-item">
                        <span>
                            کاربران آفلاین
                        </span>
                            <div class="switch">
                                <div class="onoffswitch">
                                    <input type="checkbox" checked name="collapsemenu" class="onoffswitch-checkbox" id="example5">
                                    <label class="onoffswitch-label" for="example5">
                                        <span class="onoffswitch-inner"></span>
                                        <span class="onoffswitch-switch"></span>
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="setings-item">
                        <span>
                            جستجوی عمومی
                        </span>
                            <div class="switch">
                                <div class="onoffswitch">
                                    <input type="checkbox" checked name="collapsemenu" class="onoffswitch-checkbox" id="example6">
                                    <label class="onoffswitch-label" for="example6">
                                        <span class="onoffswitch-inner"></span>
                                        <span class="onoffswitch-switch"></span>
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="setings-item">
                        <span>
                            بروزرسانی روزانه
                        </span>
                            <div class="switch">
                                <div class="onoffswitch">
                                    <input type="checkbox" name="collapsemenu" class="onoffswitch-checkbox" id="example7">
                                    <label class="onoffswitch-label" for="example7">
                                        <span class="onoffswitch-inner"></span>
                                        <span class="onoffswitch-switch"></span>
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="sidebar-content">
                            <h4>تنظیمات</h4>
                            <div class="small">
                                لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است.
                            </div>
                        </div>

                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection

@section('scripts')

@endsection

