@extends('user_side/layout/card_pages_layout')
@section('meta_data')

@endsection
@section('title')
    <title>Title</title>
@endsection
@section('styles')
    <style>
        .table
        {
            direction: rtl;
        }
    </style>
@endsection
@section('contents')
    <div class="container">
        <table class="table table-bordered table-hover">
            <tr>
                <th>ردیف</th>
                <th>عکس پروفایل</th>
                <th>نام کامل</th>
                <th>تعداد دنبال کننده</th>
                <th></th>
            </tr>
            @foreach($top10s as $index => $top10)
                <tr>
                    <td>{{$index+1}}</td>
                    <td><img src="{{$top10->profile_pic_url}}" style="border-radius: 100%;width : 60px;height:60px;" alt="{{$top10->username}}" ></td>
                    <td>{{$top10->full_name}}</td>
                    <td>{{number_format($top10->followed_by_count)}}</td>
                    <td><a href="{{url('instagram/page').'/'.$top10->page_id}}"><button>مشاهده</button></a></td>
                </tr>
            @endforeach
        </table>
    </div>
@endsection

@section('footer')

@endsection
@section('scripts')
    <script>

    </script>
@endsection
