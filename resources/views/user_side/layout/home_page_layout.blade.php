<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    @yield('meta_data')
    @yield('title')
    <link href="{{asset('user_side/styles/norm.css')}}" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <link href="{{asset('user_side/styles/overall/styles.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('user_side/styles/home_page/styles.css')}}" rel="stylesheet" type="text/css">
    @yield('styles')
</head>
<body onload="{{(isset($slider)) ? "st_slider('".asset('user_side')."')" : ""}}">
@yield('contents')
<footer>
    @yield('footer')
</footer>
<script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
@yield('scripts')
</body>
</html>
