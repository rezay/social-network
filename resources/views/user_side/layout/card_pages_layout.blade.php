<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    @yield('meta_data')
    @yield('title')
    <link href="{{asset('user_side/styles/norm.css')}}" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <link href="{{asset('user_side/styles/overall/styles.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('user_side/styles/card_pages/styles.css')}}" rel="stylesheet" type="text/css" />
    @yield('styles')

</head>
<body onload="{{(isset($slider)) ? "st_slider('".asset('user_side')."')" : ""}}">
    <div id="card">
        <a href="{{url('instagram')}}"><img src="{{asset('user_side/pics/close_icon.png')}}" id="close_icon" alt="close"></a>
        <a href="{{url('instagram')}}"><img src="{{asset('user_side/pics/home_icon.png')}}" id="home_icon" alt="home"></a>
        @yield('contents')
    </div>
<script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
@yield('scripts')
</body>
</html>
