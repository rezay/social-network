@extends('user_side/layout/card_pages_layout')
@section('meta_data')

@endsection
@section('title')
    <title>Title</title>
@endsection
@section('styles')
    <style>
        #comparision_chart{
            weight : 100%;
            height: 600px
        }
        #chartdiv > div:nth-child(2) > svg:nth-child(1) > g:nth-child(3) > g:nth-child(2) > g:nth-child(1) > g:nth-child(1) > g:nth-child(1) > g:nth-child(1) > g:nth-child(1) > g:nth-child(1) > g:nth-child(3)
        {
            display : none;
        }
        #chartdiv > div:nth-child(2) > svg:nth-child(1) > g:nth-child(3) > g:nth-child(2) > g:nth-child(1) > g:nth-child(1) > g:nth-child(1) > g:nth-child(1) > g:nth-child(1) > g:nth-child(1) > g:nth-child(1) > g:nth-child(1) > g:nth-child(2) > g:nth-child(1) > g:nth-child(1) > g:nth-child(1) > g:nth-child(3)
        {
            display: none;
        }
        #chartdiv > div:nth-child(1) > svg:nth-child(1) > g:nth-child(3) > g:nth-child(2) > g:nth-child(1) > g:nth-child(1) > g:nth-child(1) > g:nth-child(1) > g:nth-child(1) > g:nth-child(1) > g:nth-child(1) > g:nth-child(1) > g:nth-child(2) > g:nth-child(1) > g:nth-child(1) > g:nth-child(1) > g:nth-child(3)
        {
            display : none;
        }
        #searched_usernames
        {
            height:200px;overflow: auto;font-size : 12pt
        }
        .pills_buttons
        {
            margin: 0 2px ;
            font-size : 12pt
        }
    </style>
@endsection
@section('contents')
    <div class="container-fluid">
        <header>
            <p style="direction : rtl;text-align: center;font-family: yekan,serif;font-size : 14pt" class="alert alert-info">برای مقایسه نام کاربری را وارد کنید</p>
            <input name="username" id="search_input" style="text-align: left;font-family: yekan,serif;font-size: 10pt" class="form-control" onkeyup="search_username(this.value)">
            <div id="searched_usernames" class="list-group">
            </div>
        </header>
        <section id="chart">
            <div id="username_pills"></div>
            <div class="loader">
                <span>Loading...</span>
            </div>
            <div id="comparision_chart"></div>
        </section>
    </div>
@endsection

@section('footer')

@endsection
@section('scripts')
    <script src="https://www.amcharts.com/lib/4/core.js"></script>
    <script src="https://www.amcharts.com/lib/4/charts.js"></script>
    <script src="https://www.amcharts.com/lib/4/themes/animated.js"></script>
    <script>
        $(document).ready(function(){
            $("#search_input").prop("disabled", true);
            $("body").click(function () {
                $("#searched_usernames").empty();
            });
        })
    </script>
    <script>
        let usernames = chart_data =[];
        $.get("{{url('api/v1/instagram/page/username-search')}}" , function (response) {
            if (response.http_code === 200) {
                $("#searched_usernames").empty();
                usernames = response.data;
                $("#search_input").prop("disabled", false);
            }
        });

        function search_username(searched_word)
        {
            $("#searched_usernames").empty();
            usernames.filter(function(item){
                if(item['username'].search(searched_word) !== -1)
                {
                    $("#searched_usernames").append("<a href='' onclick='insert_new_page("+item.id+",event)' class='list-group-item list-group-item-action'>"+item.username+"</a>");
                }
            });
        }

        function get_random_color() {
            let letters = '0123456789ABCDEF';
            let color = '#';
            for (let i = 0; i < 6; i++) {
                color += letters[Math.floor(Math.random() * 16)];
            }
            return color;
        }
        function timeConverter(UNIX_timestamp){
            let a = new Date(UNIX_timestamp * 1000);
            let months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
            let year = a.getFullYear();
            // let month = months[a.getMonth()];
            let month = a.getMonth();
            let date = a.getDate();
            let hour = a.getHours();
            let min = a.getMinutes();
            let sec = a.getSeconds();
            a.setHours(0,0,0,0);
            // return date + ' ' + month + ' ' + year + ' ' + hour + ':' + min + ':' + sec;
            return  year + '-' + month + '-' + date ;
            // return date + ' ' + month + ' ' + year + ' ';
        }
        function insert_new_page(page_id,e)
        {
            e.preventDefault();

            $("#comparision_chart").empty();
            $(".loader").css('display' , 'block');
            $("#username_pills").empty();
            $.get("{{url('api/v1/instagram/page-data')}}"+"/"+page_id , function (response) {
                if (response.http_code === 200) {
                    $(".loader").css('display' , 'none');
                    chart_data.push(response.data);
                    for(let index in chart_data)
                    {
                        $("#username_pills").append("<button type='button' onclick='remove_page("+chart_data[index].id+")' class='pills_buttons btn btn-primary'><span class='badge badge-light'>X</span><span> "+chart_data[index].username+" </span> </button>");
                    }
                    draw_chart();
                }
            });
        }
        function remove_page(id) {
            $("#comparision_chart").empty();
            $("#username_pills").empty();

            chart_data = chart_data.filter(function(item, index, arr){
                if(item.id !== id)
                {
                    $("#username_pills").append("<button type='button' onclick='remove_page("+item.id+")' class='pills_buttons btn btn-primary'><span class='badge badge-light'>X</span><span> "+item.username+" </span> </button>");
                    return item;
                }
            });
            draw_chart();
        }
        function draw_chart()
        {
            am4core.useTheme(am4themes_animated);
            // Themes end

            // Create chart instance
            let chart = am4core.create("comparision_chart", am4charts.XYChart);

            let dates = [];
            // Add data
            chart_data.forEach(function (item , i) {
                item['page_data'].forEach(function (page_data , j) {
                    dates.push(page_data['created_at']);
                });
            });

            let data =[];
            let unique_dates = [];
            dates = dates.sort(function(a, b){ return a - b});
            dates = dates.map(function(item){return timeConverter(item);});
            dates.forEach(function(item){  if(unique_dates.indexOf(item) === -1){unique_dates.push(item)}});
            unique_dates.forEach(function (date , x) {
                let obj = {};
                obj["date"] = date;
                chart_data.forEach(function (item , i) {
                    item['page_data'].forEach(function (page_data , j) {
                        let date_x = timeConverter(page_data['created_at']);
                        if(date_x === date)
                        {
                            obj["followers"+i] = item['username'] +" : "+ page_data['followed_by_count'];
                        }
                    });
                });
                data.push(obj);
            });

            chart.data = data;

            // Set input format for the dates
            chart.dateFormatter.inputDateFormat = "yyyy-MM-dd";

            // Create axes
            let dateAxis = chart.xAxes.push(new am4charts.DateAxis());
            let valueAxis = chart.yAxes.push(new am4charts.ValueAxis());

            for(let index in chart_data)
            {
                // Create series
                let color = get_random_color();
                window["series"+index] = chart.series.push(new am4charts.LineSeries());
                window["series"+index].dataFields.valueY = "followers"+index;
                window["series"+index].dataFields.dateX = "date";
                window["series"+index].tooltipText = "{followers"+index+"}";
                window["series"+index].strokeWidth = 2;
                window["series"+index].stroke = am4core.color(color);
                window["series"+index].minBulletDistance = 15;

                // Drop-shaped tooltips
                window["series"+index].tooltip.background.cornerRadius = 20;
                window["series"+index].tooltip.background.strokeOpacity = 0;
                window["series"+index].tooltip.pointerOrientation = "vertical";
                window["series"+index].tooltip.label.minWidth = 40;
                window["series"+index].tooltip.label.minHeight = 40;
                window["series"+index].tooltip.label.textAlign = "middle";
                window["series"+index].tooltip.label.textValign = "middle";

                // Make bullets grow on hover
                window["bullet"+index] = window["series"+index].bullets.push(new am4charts.CircleBullet());
                window["bullet"+index].circle.strokeWidth = 2;
                window["bullet"+index].circle.radius = 4;
                window["bullet"+index].circle.fill = am4core.color("#fff");

                window["bullethover"+index] = window["bullet"+index].states.create("hover");
                window["bullethover"+index].properties.scale = 1.3;

            }


            // Make a panning cursor
            chart.cursor = new am4charts.XYCursor();
            chart.cursor.behavior = "panXY";
            chart.cursor.xAxis = dateAxis;
            chart.cursor.snapToSeries = series0;

            // Create vertical scrollbar and place it before the value axis
            chart.scrollbarY = new am4core.Scrollbar();
            chart.scrollbarY.parent = chart.leftAxesContainer;
            chart.scrollbarY.toBack();

            // Create a horizontal scrollbar with previe and place it underneath the date axis
            chart.scrollbarX = new am4charts.XYChartScrollbar();
            chart.scrollbarX.series.push(series0);
            chart.scrollbarX.parent = chart.bottomAxesContainer;

            dateAxis.start = 0.79;
            dateAxis.keepSelection = true;
        }
        // function draw_chart2()
        // {
        //     am4core.ready(function() {
        //
        //         am4core.useTheme(am4themes_animated);
        //         // Themes end
        //
        //         // Create chart
        //         let chart = am4core.create("chartdiv", am4charts.XYChart);
        //
        //         let data = [];
        //         let quantity = 30000;
        //         for(let index in chart_data)
        //         {
        //             for(let i = 0; i < chart_data[index]['page_data'].length ; i++)
        //             {
        //                 let date = timeConverter(chart_data[index]['page_data'][i]['created_at']);
        //                 let obj = {};
        //                 obj["date"+index] = date;
        //                 obj["followers"+index] = chart_data[index]['page_data'][i]['followed_by_count'];
        //                 data.push(obj);
        //             }
        //         }
        //
        //         chart.data = data;
        //
        //         for(let index in chart_data)
        //         {
        //             let color = get_random_color();
        //             window["dateAxis"+index] = chart.xAxes.push(new am4charts.DateAxis());
        //             window["dateAxis"+index].renderer.grid.template.location = 0;
        //             window["dateAxis"+index].renderer.labels.template.fill = am4core.color(color);
        //
        //             window["valueAxis"+index] = chart.yAxes.push(new am4charts.ValueAxis());
        //             window["valueAxis"+index].tooltip.disabled = true;
        //             window["valueAxis"+index].renderer.labels.template.fill = am4core.color(color);
        //             window["valueAxis"+index].renderer.minWidth = 60;
        //
        //             window["series"+index] = chart.series.push(new am4charts.LineSeries());
        //             window["series"+index].name = chart_data[index].username;
        //             window["series"+index].dataFields.dateX = "date"+index;
        //             window["series"+index].dataFields.valueY = "followers"+index;
        //             if(index == 1)
        //             {
        //                 // ("series"+1).yAxis = ("valueAxis"+1);
        //                 // ("series"+1).xAxis = ("dateAxis"+1);
        //                 // window["valueAxis"+index].renderer.grid.template.strokeDasharray = "2,3";
        //
        //             }
        //
        //             window["series"+index].tooltipText = "{valueY.value}";
        //             window["series"+index].fill = am4core.color(color);
        //             window["series"+index].stroke = am4core.color(color);
        //             //series.strokeWidth = 3;
        //
        //         }
        //
        //         chart.cursor = new am4charts.XYCursor();
        //         chart.cursor.xAxis = window["valueAxis"+(chart_data.length-1)];
        //         // chart.cursor.yAxis = window["valueAxis"+(chart_data.length-1)];
        //
        //         let scrollbarX = new am4charts.XYChartScrollbar();
        //         scrollbarX.series.push(window["series"+(chart_data.length-1)]);
        //         chart.scrollbarX = scrollbarX;
        //
        //         chart.legend = new am4charts.Legend();
        //         chart.legend.parent = chart.plotContainer;
        //         chart.legend.zIndex = 100;
        //
        //
        //         for(let index in chart_data)
        //         {
        //             window["valueAxis"+index].renderer.grid.template.strokeOpacity = 0.07;
        //             window["dateAxis"+index].renderer.grid.template.strokeOpacity = 0.07;
        //         }
        //
        //
        //     }); // end am4core.ready()
        // }
    </script>
@endsection
