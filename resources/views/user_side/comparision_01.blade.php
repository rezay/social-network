@extends('user_side/layout/user_side_layout')
@section('meta_data')

@endsection
@section('title')
    <title>Title</title>
@endsection
@section('styles')
    <link href="{{asset('user_side/styles/page_data.css')}}" type="text/css" rel="stylesheet">
@endsection
@section('contents')
<div  style="font-family:yekan, serif;border-radius: 20px;box-shadow: 0 0 2px 3px rgba(1,1,1,0.1);margin : 10px;padding : 30px 5px">
    <a href="{{url('instagram')}}"><img src="{{asset('user_side/pics/close_icon.png')}}" id="close_icon" alt="close"></a>
    <main>
        <div class="container">
            <p style="direction : rtl;text-align: center;font-family: yekan,serif;font-size : 14pt" class="alert alert-info">برای مقایسه نام کاربری را وارد کنید</p>
            <input name="username" style="text-align: left;font-family: yekan,serif;font-size: 10pt" class="form-control" onkeyup="search_username(this.value)">
            <div id="searched_usernames" class="list-group" style="height:200px;overflow: auto;font-size : 12pt">
            </div>
        </div>
        <div id="username_pills"></div>
        <div class="loader" style="display: none"></div>
        <div id="chartdiv" style="weight : 100%;height: 500px"></div>

    </main>
</div>
@endsection

@section('footer')

@endsection
@section('scripts')
    <script src="https://cdn.zingchart.com/zingchart.min.js"></script>
    <script>
        $(document).ready(function(){
            $("body").click(function () {
                $("#searched_usernames").empty();
            });
        })
    </script>
    <script>
        function search_username(username)
        {
            if(username.length > 1)
            {
                $.get("{{url('api/v1/instagram/page/username-search?searched_username=')}}"+username , function (response) {
                    if (response.http_code === 200) {
                        $("#searched_usernames").empty();
                        let username = response.data;
                        for(let index in username)
                        {
                            $("#searched_usernames").append("<a href='' onclick='insert_new_page("+username[index].id+",event)' class='list-group-item list-group-item-action'>"+username[index].username+"</a>");
                        }
                    }
                });
            }
        }
        let chart_data = [];

        function get_random_color() {
            let letters = '0123456789ABCDEF';
            let color = '#';
            for (let i = 0; i < 6; i++) {
                color += letters[Math.floor(Math.random() * 16)];
            }
            return color;
        }
        function timeConverter(UNIX_timestamp){
            let a = new Date(UNIX_timestamp * 1000);
            let months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
            let year = a.getFullYear();
            let month = months[a.getMonth()];
            let date = a.getDate();
            let hour = a.getHours();
            let min = a.getMinutes();
            let sec = a.getSeconds();
            a.setHours(0,0,0,0);
            // return date + ' ' + month + ' ' + year + ' ' + hour + ':' + min + ':' + sec;
            return date + ' ' + month + ' ' + year + ' ';
        }
        function insert_new_page(page_id,e)
        {
            e.preventDefault();
            $("#chartdiv").empty();
            $(".loader").css('display' , 'block');
            $("#username_pills").empty();
            $.get("{{url('api/v1/instagram/page-data')}}"+"/"+page_id , function (response) {
                if (response.http_code === 200) {
                    // Themes begin


                    $(".loader").css('display' , 'none');
                    chart_data.push(response.data);
                    for(let index in chart_data)
                    {
                        $("#username_pills").append("<button type='button' style='margin: 0 2px ;font-size : 12pt' onclick='remove_page("+index+")' class='btn btn-primary'><span class='badge badge-light'>X</span><span> "+chart_data[index].username+" </span> </button>");
                    }
                    draw_chart();
                }
            });
        }
        function remove_page(id) {
            $("#chartdiv").empty();
            $(".loader").css('display' , 'block');
            $("#username_pills").empty();

            $(".loader").css('display' , 'none');
            chart_data = chart_data.filter(function(value, index, arr){

                if(index !== id)
                {
                    return value;
                }
            });
            for(let index in chart_data)
            {
                $("#username_pills").append("<button type='button' style='margin: 0 2px ;font-size : 12pt' onclick='remove_page("+index+")' class='btn btn-primary'><span class='badge badge-light'>X</span><span> "+chart_data[index].username+" </span> </button>");
            }
            draw_chart();
        }
        function draw_chart()
        {
            let series = [];
            for(let i in chart_data)
            {
                let color = get_random_color();
                let values = [];
                let page_data = chart_data[i]['page_data'];
                for(let j in page_data)
                {
                    values.push(page_data[j]['followed_by_count']);
                }
                series.push({
                    values: values,
                    lineColor: color,
                    marker: {
                        backgroundColor: color
                    }
                });
            }

            let chartConfig = {
                type: 'line',
                backgroundColor: '#2C2C39',
                // title: {
                //     text: 'Time Series Data with null values',
                //     adjustLayout: true,
                //     marginTop: '7px',
                //     fontColor: '#E3E3E5'
                // },
                legend: {
                    align: 'center',
                    backgroundColor: 'none',
                    borderWidth: '0px',
                    item: {
                        cursor: 'hand',
                        fontColor: '#E3E3E5'
                    },
                    marker: {
                        type: 'circle',
                        borderWidth: '0px',
                        cursor: 'hand'
                    },
                    verticalAlign: 'top'
                },
                plot: {
                    aspect: 'spline',
                    lineWidth: '2px',
                    marker: {
                        borderWidth: '0px',
                        size: '5px'
                    }
                },
                plotarea: {
                    margin: 'dynamic 70'
                },
                scaleX: {
                    item: {
                        fontColor: '#E3E3E5'
                    },
                    lineColor: '#E3E3E5',
                    minValue: 1459468800000,
                    step: 'day',
                    transform: {
                        type: 'date',
                        all: '%y %M %d'
                    },
                    zooming: true,
                    zoomTo: [0, 15]
                },
                scaleY: {
                    // guide: {
                    //     lineStyle: 'dashed'
                    // },
                    item: {
                        fontColor: '#E3E3E5'
                    },
                    lineColor: '#E3E3E5',
                    // minorGuide: {
                    //     alpha: 0.7,
                    //     lineColor: '#E3E3E5',
                    //     lineStyle: 'dashed',
                    //     lineWidth: '1px',
                    //     visible: true
                    // },
                    minorTick: {
                        lineColor: '#E3E3E5'
                    },
                    minorTicks: 1,
                    tick: {
                        lineColor: '#E3E3E5'
                    }
                },
                crosshairX: {
                    marker: {
                        alpha: 0.5,
                        size: '7px'
                    },
                    plotLabel: {
                        borderRadius: '3px',
                        multiple: true
                    },
                    scaleLabel: {
                        backgroundColor: '#53535e',
                        borderRadius: '3px'
                    }
                },
                crosshairY: {
                    type: 'multiple',
                    lineColor: '#E3E3E5',
                    scaleLabel: {
                        bold: true,
                        borderRadius: '3px',
                        decimals: 2,
                        fontColor: '#2C2C39',
                        offsetX: '-5px'
                    }
                },
                shapes: [
                    {
                        type: 'rectangle',
                        id: 'view_all',
                        backgroundColor: '#53535e',
                        borderColor: '#E3E3E5',
                        borderRadius: '3px',
                        borderWidth: '1px',
                        cursor: 'hand',
                        label: {
                            text: 'View All',
                            bold: true,
                            fontColor: '#E3E3E5',
                            fontSize: '12px'
                        },
                        width: '75px',
                        height: '20px',
                        x: '85%',
                        y: '7%'
                    }
                ],
                tooltip: {
                    borderRadius: '3px',
                    borderWidth: '0px'
                },
                preview: {
                    adjustLayout: true,
                    borderColor: '#E3E3E5',
                    label: {
                        fontColor: '#E3E3E5'
                    },
                    mask: {
                        backgroundColor: '#E3E3E5'
                    }
                },
                series: series
            };

            zingchart.bind('chartdiv', 'shape_click', function (p) {
                if (p.shapeid == 'view_all') {
                    zingchart.exec(p.id, 'viewall');
                }
            });

            zingchart.render({
                id: 'chartdiv',
                data: chartConfig
            });
        }
    </script>
@endsection
