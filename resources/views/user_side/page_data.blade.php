@extends('user_side/layout/card_pages_layout')
@section('meta_data')

@endsection
@section('title')
    <title>Title</title>
@endsection
@section('styles')
    <link href="{{asset('user_side/styles/page_data/styles.css')}}" rel="stylesheet" type="text/css" />
    <style>
        .chart
        {
            weight : 100%;height: 500px
        }
        #followers_chart
        {

        }
        .profile
        {
            font-family: sans-serif,yekan, serif;
            font-weight: 100;
        }
        .profile-image
        {
            background-image: radial-gradient(circle at 30% 107%, #fdf497 0%, #fdf497 5%, #fd5949 45%, #d6249f 60%, #285AEB 90%);
            border-radius: 50%;
            width: 14vw;
            height: 14vw;
        }
        .profile-image img
        {
            width : 96%;
        }
        .profile-stats{
            line-height: 40px;
        }
        .profile-stat-count
        {
            font-weight: bold;
        }
        .media_btn
        {
            display : block;
            padding : 4px;
            background-color: rgba(10,200,10 , 0.4);
            text-decoration: none;
        }
        .media_btn:hover
        {
            background-color: rgba(10,250,10 , 0.6);
            text-decoration: none;
        }
    </style>
@endsection
@section('contents')
<header>
    <div class="container">
        <div class="profile row py-5">
            <div class="col-lg-4 col-sm-12 d-flex justify-content-center">
                <div class="row">
                    <div class="profile-image d-flex justify-content-center align-items-center">
                        <img src="{{$page['latest_page_data']->profile_pic_url}}" class="rounded-circle" alt="profile image">
                    </div>
                </div>
            </div>
            <div class="profile-stats col-lg-8 col-sm-12 py-1 px-4">
                <h1 class="profile-user-name col-12"> {{$page->username}} </h1>
                <div class="align-content-center row">
                    <div class="col"><span class="profile-stat-count">{{$page['latest_page_data']->media_count}}</span> posts</div>
                    <div class="col"><span class="profile-stat-count">{{$page['latest_page_data']->followed_by_count}}</span> followers</div>
                    <div class="col"><span class="profile-stat-count">{{$page['latest_page_data']->follows_count}}</span> following</div>
                </div>
                <div>
                    <div class="full_name">{{$page['latest_page_data']->full_name}}</div>
                    <div class="bio">{{$page['latest_page_data']->biography}}</div>
                    <a href="{{$page['latest_page_data']->external_url}}" class="link">{{$page['latest_page_data']->external_url}}</a>
                </div>
            </div>

        </div>
    </div>
</header>

<main>
    <div class="container-fluid">
        <div class="row profile_medias">
            @foreach($medias as $media)
                <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
                    <div class="hovereffect">
                        <img class="img-responsive" src="{{$media['image_thumbnail_url']}}" height="320" width="320" alt="">
                        <div class="overlay">
                            <h2><span></span> {{$media['likes_count']}}</h2>
                            <h2><span></span> {{$media['comments_count']}}</h2>
                            <p>
                                <a href="http://instagram.com/p/{{$media['short_code']}}" class="media_btn" target="_blank">open on instagram</a>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="clearfix visible-md-block"></div>
            @endforeach
        </div>
        <div class="alert alert-info text-center mt-5">current followers  : <span id="current followers"></span></div>
        <div class="loader" id="followers_loader">
            <span>Loading...</span>
        </div>
        <div id="followers_chart" class="chart"></div>

        <div class="alert alert-info text-center">interest rate  : <span id="latest_interest_rate_with_comments_and_likes"></span></div>
        <div class="loader" id="interest_rate_with_comments_and_likes_loader">
            <span>Loading...</span>
        </div>
        <div id="interest_rate_with_comments_and_likes_chart" class="chart"></div>

        <div class="alert alert-info text-center">interest rate  : <span id="latest_interest_rate_with_videos"></span></div>
        <div class="loader" id="interest_rate_with_video_loader">
            <span>Loading...</span>
        </div>
        <div id="interest_rate_with_video_chart" class="chart"></div>
    </div>
</main>
@endsection

@section('footer')

@endsection
@section('scripts')

    <script src="https://www.amcharts.com/lib/4/core.js"></script>
    <script src="https://www.amcharts.com/lib/4/charts.js"></script>
    <script src="https://www.amcharts.com/lib/4/themes/animated.js"></script>
    <script>
        function timeConverter(UNIX_timestamp){
            let a = new Date(UNIX_timestamp * 1000);
            let months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
            let year = a.getFullYear();
            let month = months[a.getMonth()];
            let date = a.getDate();
            let hour = a.getHours();
            let min = a.getMinutes();
            let sec = a.getSeconds();
            a.setHours(0,0,0,0);
            // return date + ' ' + month + ' ' + year + ' ' + hour + ':' + min + ':' + sec;
            return date + ' ' + month + ' ' + year + ' ';
        }

        function draw_followers_chart(response)
        {
            am4core.ready(function() {

                // Themes begin
                am4core.useTheme(am4themes_animated);
                // Themes end

                let chart = am4core.create("followers_chart", am4charts.XYChart);

                let data = [];
                let value = 50;
                let r_data = response.data['page_data'];
                for(let i = 0; i < r_data.length; i++){
                    let date = timeConverter(r_data[i]['created_at']);
                    value = r_data[i]['followed_by_count'];
                    data.push({date:date, value: value});
                }
                chart.data = data;

                // Create axes
                let dateAxis = chart.xAxes.push(new am4charts.DateAxis());
                dateAxis.renderer.minGridDistance = 60;

                let valueAxis = chart.yAxes.push(new am4charts.ValueAxis());

                // Create series
                let series = chart.series.push(new am4charts.LineSeries());
                series.dataFields.valueY = "value";
                series.dataFields.dateX = "date";
                series.tooltipText = "{value}";

                series.tooltip.pointerOrientation = "vertical";

                chart.cursor = new am4charts.XYCursor();
                chart.cursor.snapToSeries = series;
                chart.cursor.xAxis = dateAxis;

                //chart.scrollbarY = new am4core.Scrollbar();
                chart.scrollbarX = new am4core.Scrollbar();

            }); // end am4core.ready()
        }

        function draw_interest_rate_with_comments_and_likes_chart(interest_rates)
        {
            am4core.useTheme(am4themes_animated);
            // Themes end

            // Create chart instance
            let chart = am4core.create("interest_rate_with_comments_and_likes_chart", am4charts.XYChart3D);

            // Add data
            let data = [];
            interest_rates.forEach(function (interest_rate , i) {
                if(interest_rate.type === 1)
                {
                    data.push({
                        "date" : interest_rate.date,
                        "interest_rate" : interest_rate.rate
                    });
                }
            });
            chart.data = data;

            // Create axes
            let categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
            categoryAxis.dataFields.category = "date";
            categoryAxis.renderer.labels.template.rotation = 270;
            categoryAxis.renderer.labels.template.hideOversized = false;
            categoryAxis.renderer.minGridDistance = 20;
            categoryAxis.renderer.labels.template.horizontalCenter = "right";
            categoryAxis.renderer.labels.template.verticalCenter = "middle";
            categoryAxis.tooltip.label.rotation = 270;
            categoryAxis.tooltip.label.horizontalCenter = "right";
            categoryAxis.tooltip.label.verticalCenter = "middle";

            let valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
            valueAxis.title.text = "Interest Rate";
            valueAxis.title.fontWeight = "bold";

            // Create series
            let series = chart.series.push(new am4charts.ColumnSeries3D());
            series.dataFields.valueY = "interest_rate";
            series.dataFields.categoryX = "date";
            series.name = "Interest Rate";
            series.tooltipText = "[bold]{valueY}[/]";
            series.columns.template.fillOpacity = .8;

            let columnTemplate = series.columns.template;
            columnTemplate.strokeWidth = 2;
            columnTemplate.strokeOpacity = 1;
            columnTemplate.stroke = am4core.color("#FFFFFF");

            columnTemplate.adapter.add("fill", function(fill, target) {
                return chart.colors.getIndex(target.dataItem.index);
            });

            columnTemplate.adapter.add("stroke", function(stroke, target) {
                return chart.colors.getIndex(target.dataItem.index);
            });

            chart.cursor = new am4charts.XYCursor();
            chart.cursor.lineX.strokeOpacity = 0;
            chart.cursor.lineY.strokeOpacity = 0;
        }

        function draw_interest_rate_with_video_chart(interest_rates)
        {
            am4core.useTheme(am4themes_animated);
            // Themes end

            // Create chart instance
            let chart = am4core.create("interest_rate_with_video_chart", am4charts.XYChart3D);

            let data = [];
            interest_rates.forEach(function (interest_rate , i) {
                if(interest_rate.type ===2)
                {
                    data.push({
                        "date" : interest_rate.date,
                        "interest_rate" : interest_rate.rate
                    });
                }
            });

            // Add data
            chart.data = data;

            // Create axes
            let categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
            categoryAxis.dataFields.category = "date";
            categoryAxis.renderer.labels.template.rotation = 270;
            categoryAxis.renderer.labels.template.hideOversized = false;
            categoryAxis.renderer.minGridDistance = 20;
            categoryAxis.renderer.labels.template.horizontalCenter = "right";
            categoryAxis.renderer.labels.template.verticalCenter = "middle";
            categoryAxis.tooltip.label.rotation = 270;
            categoryAxis.tooltip.label.horizontalCenter = "right";
            categoryAxis.tooltip.label.verticalCenter = "middle";

            let valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
            valueAxis.title.text = "Interest Rate";
            valueAxis.title.fontWeight = "bold";

            // Create series
            let series = chart.series.push(new am4charts.ColumnSeries3D());
            series.dataFields.valueY = "interest_rate";
            series.dataFields.categoryX = "date";
            series.name = "Interest Rate";
            series.tooltipText = "[bold]{valueY}[/]";
            series.columns.template.fillOpacity = .8;

            let columnTemplate = series.columns.template;
            columnTemplate.strokeWidth = 2;
            columnTemplate.strokeOpacity = 1;
            columnTemplate.stroke = am4core.color("#FFFFFF");

            columnTemplate.adapter.add("fill", function(fill, target) {
                return chart.colors.getIndex(target.dataItem.index);
            });

            columnTemplate.adapter.add("stroke", function(stroke, target) {
                return chart.colors.getIndex(target.dataItem.index);
            });

            chart.cursor = new am4charts.XYCursor();
            chart.cursor.lineX.strokeOpacity = 0;
            chart.cursor.lineY.strokeOpacity = 0;

        }

        let charts_loaded = {"followers_chart" : 0 ,"interest_rate_with_comments_and_likes_chart" : 0 ,"interest_rate_with_video_chart" : 0 };
        function scroll_events() {
            let window_top = $(window).scrollTop();
            let followers_chart_top = $('#followers_chart').offset().top;
            let interest_rate_with_comments_and_likes_chart = $("#interest_rate_with_comments_and_likes_chart").offset().top;
            let interest_rate_with_video_chart = $("#interest_rate_with_video_chart").offset().top;
            if(window_top > (followers_chart_top - window.innerHeight/2) && charts_loaded.followers_chart === 0)
            {
                charts_loaded.followers_chart = 1;
                $.get("{{url('api/v1/instagram/page-data',$page_id)}}" , function (response) {
                    if(response.http_code === 200)
                    {
                        $("#followers_loader").css('display' , 'none');
                        draw_followers_chart(response);
                    }
                });
            }
            if(window_top  > (interest_rate_with_comments_and_likes_chart - window.innerHeight/2) && charts_loaded.interest_rate_with_comments_and_likes_chart === 0)
            {
                charts_loaded.interest_rate_with_comments_and_likes_chart = 1;
                $.get("{{url('api/v1/instagram/page/interest-rates')}}" , {page_id : "{{$page_id}}"} , function (response) {
                    if(response.http_code === 200)
                    {
                        let interest_rates = response.data;
                        $("#interest_rate_with_comments_and_likes_loader").css('display' , 'none');
                        draw_interest_rate_with_comments_and_likes_chart(interest_rates);
                    }
                });
            }
            if(window_top  > (interest_rate_with_video_chart - window.innerHeight/2) && charts_loaded.interest_rate_with_video_chart === 0)
            {
                charts_loaded.interest_rate_with_video_chart = 1;
                $.get("{{url('api/v1/instagram/page/interest-rates')}}" , {page_id : "{{$page_id}}"} , function (response) {
                    if(response.http_code === 200)
                    {
                        let interest_rates = response.data;
                        $("#interest_rate_with_video_loader").css('display' , 'none');
                        draw_interest_rate_with_video_chart(interest_rates);
                    }
                });
            }
            if(charts_loaded.followers_chart !== 0 && charts_loaded.interest_rate_with_comments_and_likes_chart !== 0 && charts_loaded.interest_rate_with_video_chart !== 0)
            {
                window.removeEventListener("scroll", scroll_events)
            }
        }
        window.addEventListener("scroll", scroll_events);

    </script>
@endsection
