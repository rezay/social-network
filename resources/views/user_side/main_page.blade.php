@extends('user_side/layout/home_page_layout')
@section('meta_data')

@endsection
@section('title')
    <title>Title</title>
@endsection
@section('styles')
    <style>
        #top_10_chart {
            width: 95%;
            margin : 0 auto;
            height: 1200px;
        }
    </style>

@endsection
@section('contents')
    <header id="header">
        <nav>
            <ul class="main_menu">
                <li><a href="#services">سرویس ها</a></li>
                <li><a href="#portfolio">گالری</a></li>
                <li><a href="#contact_us">ارتباط با ما</a></li>
                <li><a href="#top10">برترین ها</a></li>
            </ul>
        </nav>
    </header>
    <a id="scroll-top" href="#"></a>
    <nav id="steaky_nav">
        <a href="#services">سرویس ها</a>
        <a href="#portfolio">گالری</a>
        <a href="#contact_us">ارتباط با ما</a>
        <a href="#top10">برترین ها</a>
    </nav>
    <section id="services">
        <div class="container">
            <div class="p1-container">
                <h2>سرویس ها</h2>
                <p>
                    این یک متن است این یک متن است این یک متن است این یک متن است این یک متن است
                </p>
            </div>
            <div class="columns_container row justify-content-around align-items-center">
                <div class="col-lg-4">
                    <a href="{{url('instagram/page/comparision')}}" class="a_column">
                        <div class="column">
                            <div class="services_pic">
                                <img src="{{asset('user_side/pics/images/service1.png')}}" alt="" title="everybodys_got_a_secret-wallpaper">
                            </div>
                            <h3>
                                مقایسه
                            </h3>
                            <p class="services_description">
                                سیبدسیب سیبنسیب سنشس یشدیش یشنید سیبدسیب سیبنسیب سنشس یشدیش یشنید سیبدسیب سیبنسیب سنشس یشدیش یشنیدندب یتبدیب یبتی سیبدسب سبندزص ب
                            </p>
                        </div>
                    </a>
                </div>
                <div class="col-lg-4">
                    <a href="" class="a_column">
                        <div class="column">
                            <div class="services_pic">
                                <img src="{{asset('user_side/pics/images/service2.png')}}" alt="" title="flowing_paint-wallpaper">
                            </div>
                            <h3>
                                این یک متن است
                            </h3>
                            <p class="services_description">
                                مپمدسیب سبیددتذبیس سندنسمیخب صش ضضم سیبدسیب سیبنسیب سنشس یشدیش یشنید سیبدسیب سیبنسیب سنشس یشدیش یشنید سیبدسیب سیبنسیب سنشس یشدیش یشنید
                            </p>
                        </div>
                    </a>
                </div>
                <div class="col-lg-4">
                    <a href="" class="a_column">
                        <div class="column">
                            <div class="services_pic">
                                <img src="{{asset('user_side/pics/images/service3.png')}}" alt="" title="green_fabric_2-wallpaper">
                            </div>
                            <h3>
                                این یک متن است
                            </h3>
                            <p class="services_description">
                                سیبدسیب سیبنسیب سنشس یشدیش یشنید سیبدسیب سیبنسیب سنشس یشدیش یشنید سیبدسیب سیبنسیب سنشس یشدیش یشنید سیبدسیب سیبنسیب سنشس یشدیش یشنید سطسطست سسدس
                            </p>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </section>
    <section id="portfolio">
        <ul class="pictures_container">
            @foreach($favors as $index => $favor)
                <li style="background-image: url({{$favor['latest_page_data']->profile_pic_url_hd}})">
                    <a href="{{url('instagram/page',$favor->id)}}" class="pictures">
                        {{$favor['latest_page_data']->full_name}}
                    </a>
                </li>
            @endforeach
        </ul>
    </section>
    <section id="contact_us">
        <div class="container">
            <div id="info_side">
                <h1>درباره ما</h1>
                <p>
                    این مطلب در مورد این وب سایت میباشد این مطلب در مورد این وب سایت میباشد این مطلب در مورد این وب سایت میباشد این مطلب در مورد این وب سایت میباشد
                </p>
            </div>
            <div id="form_side">
                <form method="post" action="">
                    <input name="name" class="input" type="text" placeholder="اسم">
                    <input name="email" class="input" type="email" placeholder="ایمیل">
                    <textarea name="text" placeholder="پیام"></textarea>
                    <input name="submit" class="submit_input" type="submit" value="ارسال">
                </form>
            </div>
        </div>
    </section>

    <section id="top10">
        <div id="top_10_chart"></div>
        <div class="container">
            <table class="table table-bordered">
                <tr>
                    <th>ردیف</th>
                    <th>نام کاربری</th>
                    <th>نام اصلی</th>
                    <th>تعداد دنبال کننده</th>
                    <th></th>
                </tr>
                @foreach($top10s as $index => $top10)
                    <tr>
                        <td>{{$index+1}}</td>
                        <td>{{$top10->username}}</td>
                        <td>{{$top10->full_name}}</td>
                        <td>{{number_format($top10->followed_by_count)}}</td>
                        <td><a href="{{url('instagram/page').'/'.$top10->page_id}}"><button>مشاهده</button></a></td>
                    </tr>
                @endforeach
            </table>
            <div class="col-md-12">
                <a href="{{url('instagram/page/top-followed')}}"><button style="width : 100%" class="btn btn-primary">نمایش همه</button></a>
            </div>

        </div>
    </section>

@endsection

@section('footer')

@endsection
@section('scripts')
    <script type="text/javascript" src="{{asset('user_side/js/scripts.js')}}"></script>
    <script src="https://www.amcharts.com/lib/4/core.js"></script>
    <script src="https://www.amcharts.com/lib/4/charts.js"></script>
    <script src="https://www.amcharts.com/lib/4/themes/animated.js"></script>

    <!-- Chart code -->
    <script>
        let chart_data = [];

        $.get("{{url('api/v1/instagram/page/top-followed/1?page_size=10')}}" , function (response) {
            if (response.http_code === 200) {
                // Themes begin
                $(".loader").css('display' , 'none');
                chart_data = response.data;
                draw_chart();
            }
        });
        function draw_chart()
        {
            am4core.ready(function() {
                am4core.useTheme(am4themes_animated);

                let chart = am4core.create("top_10_chart", am4charts.XYChart);
                chart.hiddenState.properties.opacity = 0; // this creates initial fade-in

                chart.paddingBottom = 30;
                let data = [];
                for(let i in chart_data)
                {
                    data.unshift({
                        "name": chart_data[i]['full_name'],
                        "steps": chart_data[i]['followed_by_count'],
                        "href": chart_data[i]['profile_pic_url'],
                        "page_id" : chart_data[i]['page_id']
                    });
                }
                chart.data = data;

                let categoryAxis = chart.yAxes.push(new am4charts.CategoryAxis());
                categoryAxis.dataFields.category = "name";
                categoryAxis.renderer.grid.template.strokeOpacity = 0;
                categoryAxis.renderer.minGridDistance = 10;
                categoryAxis.renderer.labels.template.dx = -40;
                categoryAxis.renderer.minWidth = 120;
                categoryAxis.renderer.tooltip.dx = -30;

                let valueAxis = chart.xAxes.push(new am4charts.ValueAxis());
                valueAxis.renderer.inside = true;
                valueAxis.renderer.labels.template.fillOpacity = 0.3;
                valueAxis.renderer.grid.template.strokeOpacity = 0;
                valueAxis.min = 0;
                valueAxis.cursorTooltipEnabled = false;
                valueAxis.renderer.baseGrid.strokeOpacity = 0;
                valueAxis.renderer.labels.template.dy = 20;

                let series = chart.series.push(new am4charts.ColumnSeries);
                series.dataFields.valueX = "steps";
                series.dataFields.categoryY = "name";
                series.tooltipText = "{valueX.value}";
                series.tooltip.pointerOrientation = "vertical";
                series.tooltip.dy = - 30;
                series.columnsContainer.zIndex = 100;

                let columnTemplate = series.columns.template;
                columnTemplate.height = am4core.percent(50);
                columnTemplate.maxHeight = 40;
                columnTemplate.column.cornerRadius(60, 10, 60, 10);
                columnTemplate.strokeOpacity = 0;

                series.heatRules.push({ target: columnTemplate, property: "fill", dataField: "valueX", min: am4core.color("#e5dc36"), max: am4core.color("#5faa46") });
                series.mainContainer.mask = undefined;

                let cursor = new am4charts.XYCursor();
                chart.cursor = cursor;
                cursor.lineX.disabled = true;
                cursor.lineY.disabled = true;
                cursor.behavior = "none";

                let bullet = columnTemplate.createChild(am4charts.CircleBullet);
                bullet.circle.radius = 30;
                bullet.valign = "middle";
                bullet.align = "left";
                bullet.isMeasured = true;
                bullet.interactionsEnabled = false;
                bullet.horizontalCenter = "right";
                bullet.interactionsEnabled = false;

                let hoverState = bullet.states.create("hover");
                let outlineCircle = bullet.createChild(am4core.Circle);
                outlineCircle.adapter.add("radius", function (radius, target) {
                    let circleBullet = target.parent;
                    return circleBullet.circle.pixelRadius + 10;
                });

                let image = bullet.createChild(am4core.Image);
                image.width = 60;
                image.height = 60;
                image.horizontalCenter = "middle";
                image.verticalCenter = "middle";
                image.propertyFields.href = "href";

                image.adapter.add("mask", function (mask, target) {
                    let circleBullet = target.parent;
                    return circleBullet.circle;
                });

                let previousBullet;
                chart.cursor.events.on("cursorpositionchanged", function (event) {
                    let dataItem = series.tooltipDataItem;

                    if (dataItem.column) {
                        let bullet = dataItem.column.children.getIndex(1);

                        if (previousBullet && previousBullet != bullet) {
                            previousBullet.isHover = false;
                        }

                        if (previousBullet != bullet) {

                            var hs = bullet.states.getKey("hover");
                            hs.properties.dx = dataItem.column.pixelWidth;
                            bullet.isHover = true;

                            previousBullet = bullet;
                        }
                    }
                })
            }); // end am4core.ready()
        }
    </script>
@endsection

