const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/user_side/js/scripts.js', 'public/user_side/js/scripts.js')
    .copy('resources/assets/user_side/scss/norm.css' , 'public/user_side/styles/')
    .copy('resources/assets/user_side/fonts' , 'public/user_side/fonts/')
    .copy('resources/assets/user_side/pics' , 'public/user_side/pics/')
    .sass('resources/assets/user_side/scss/overall/styles.scss', 'public/user_side/styles/overall/')
    .sass('resources/assets/user_side/scss/card_pages/styles.scss', 'public/user_side/styles/card_pages/')
    .sass('resources/assets/user_side/scss/comparision/styles.scss', 'public/user_side/styles/comparision')
    .sass('resources/assets/user_side/scss/home_page/styles.scss', 'public/user_side/styles/home_page')
    .sass('resources/assets/user_side/scss/page_data/styles.scss', 'public/user_side/styles/page_data');


